
These are few sets of notes for *21-268: Multi-dimensional Calculus* taught in the [Department of Mathematical-Sciences](http://www.math.cmu.edu) at [Carnegie Mellon University](http://www.cmu.edu).

* **Fall, 2015:** *Brief* notes when taught by *Gautam Iyer*.
  (A PDF can be downloaded [here](http://www.math.cmu.edu/~gautam/sj/teaching/2015-16/268-multid-calc/pdfs/brief-notes.pdf).
  Homework and other materials can be found on the [class website](http://www.math.cmu.edu/~gautam/sj/teaching/2015-16/268-multid-calc/).)

* **Spring 2012:** Brief notes when taught by *Russell Schwab* (typed by *Christopher Almost*)
  (A PDF can be downloaded [here](http://www.math.cmu.edu/~gautam/sj/teaching/2015-16/268-multid-calc/pdfs/2012-notes.pdf).)

These notes are being distributed free of charge under the [Creative Commons Attribution-Non Commercial-Share Alike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
This means you may adapt and or redistribute this document **for non commercial purposes**, provided you give appropriate credit and re-distribute your work under the same licence.
For instance:

* You may use these as lecture notes for a similar course you are teaching, provided you follow the licence terms.

* You may give these notes to your students as a references, provided you follow the licence terms.

* You may edit these notes, and redistribute your modifications, provided you do so under the same licence.

* If however, you would like to use material in these notes for a commercial book, you must obtain written prior approval.

The full terms of this license can be found [here](http://creativecommons.org/licenses/by-nc-sa/4.0/).

If you are interested in modifying these notes, please contact the current maintainer to discuss the best method.
(You can fork it, of course; but if you are contributing regularly, it might be nice to keep a "master copy" here with all the improvements.)

These notes are provided as is, without any warranty and Carnegie Mellon University, the Department of Mathematical-Sciences, nor any of the authors are liable for any errors.
