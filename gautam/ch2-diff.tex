% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{1}\fi
  \chapter{Differentiation}\label{chrDiff}%endchapter
  \section{Directional and Partial Derivatives}

  \begin{definition}
    Let $U \subset \R^d$ be a domain, $f:U \to \R$ be a function, and $v \in \R^d - \set{0}$ be a vector.
    We define the \emph{directional derivative} of $f$ in the direction $v$ at the point $a$ by
    \begin{equation*}
      D_v f(a) \defeq \frac{d}{dt} f( a + tv ) \Bigr|_{t = 0}
    \end{equation*}
  \end{definition}

  \begin{example}
    If $f(x) = \abs{x}^2$, then $D_v f(x) = 2 x \cdot v$.
  \end{example}

  \begin{remark}
    Be aware that some authors define $D_v f$ by additionally dividing by the length of $v$.
    We will never do that!
  \end{remark}

  \begin{definition}
    We define the $i^\text{th}$ partial derivative of $f$ (denoted by $\partial_i f$) to be the directional derivative of $f$ in direction $e_i$ (where $e_i$ is the $i^{\text{th}}$ elementary basis vector).
  \end{definition}

  Practically, to compute the $i^\text{th}$ partial derivative of $f$ differentiate it with respect to $x_i$ treating all the other coordinates as constant.

  \begin{example}
    For $x \neq 0$ we have $\partial_i \abs{x} = x_i / \abs{x}$.
  \end{example}

  \section{Derivatives}

  \begin{definition}\label{dfnDf}
    Let $U \subseteq \R^d$ be a domain, $f: \R^d \to \R$ be a function, and $a \in U$.
    We say $f$ is \emph{differentiable at $a$} if there exists a linear transformation $T: \R^d \to \R$ and a function $e$ such that
    \begin{equation*}
      \lim_{h \to 0} \frac{f(a + h) - f(a) - Th}{\abs{h}} = 0\,.
    \end{equation*}
    In this case, the linear transformation $T$ is called \emph{the derivative of $f$ at $a$}, and denoted by $Df_a$.
  \end{definition}
  \begin{proposition}
    Let $U \subseteq \R^d$ be a domain, $f: \R^d \to \R$ be a function, and $a \in U$.
    The function $f$ is differentiable at $a$ \emph{if and only if} there exists a linear transformation $T: \R^d \to \R$ and a function $e$ such that
    \begin{enumerate}
      \item
	$f(a + h) = f(a) + Th + e(h)$

      \item
	\emph{and} $\lim_{h \to 0} \abs{ e(h) } / \abs{h} = 0$.
    \end{enumerate}
  \end{proposition}

  \begin{proposition}\label{ppnDf}
    If $f$ is differentiable at $a$, then all the directional derivatives $D_v f(a)$ exist.
    Further,
    \begin{equation*}
      Df_a = \begin{pmatrix}
	\partial_1 f(a) & \partial_2 f(a) & \cdots & \partial_d f(a)
      \end{pmatrix}
    \end{equation*}
    and
    \begin{equation*}
      D_v f(a) = Df_a v = \sum_{i = 1}^d v_i \partial_i f(a).
    \end{equation*}
  \end{proposition}

  \begin{remark}
    This shows that the linear transformation appearing in the definition of $f$ is unique!
  \end{remark}

  The converse of Proposition~\ref{ppnDf} is (surprisingly?) false.
  All directional derivatives can exist, however, the function need not be differentiable (or even continuous!)
  \begin{example}
    Let $f(x, y) = x^2 y / (x^4 + y^2)$. Then for every $v \in \R^2 - \set{0}$, $D_v f(0)$ exists, but $f$ is not differentiable (or even continuous) at $0$.
  \end{example}

  The converse of Proposition~\ref{ppnDf} is true under the additional assumption that the partial derivatives are continuous.
  \begin{theorem}
    If all partial derivatives of $f$ exist in a neighbourhood of $a$, \emph{and are continuous at $a$}, then $f$ is differentiable at $a$.
  \end{theorem}
  \begin{proof}
    For simplicity we assume $d = 2$.
    By the mean value theorem
    \begin{align*}
      f(a + h) - f(a)
	&= f(a_1 + h_1, a_2 + h_2) - f(a_1 + h_1, a_2) + f(a_1 + h_1, a_2) - f(a_1, a_2)
	\\
	&= h_2 \partial_2 f(a_1 + h_1, a_2 + \xi_2) + h_1 \partial_1 f(a_1 + \xi_1, a_2)
    \end{align*}
    for some $\xi_1, \xi_2$ such that $\xi_i$ lies between $0$ and $h_i$.
    Now let $T$ be the matrix $(\partial_1 f(a) ~~ \partial_2 f(a) )$ and observe
    \begin{equation*}
      f(a+h) = f(a) + Th + e(h),
    \end{equation*}
    where
    \begin{equation*}
      e(h) =
	h_2 (\partial_2 f(a_1 + h_1, a_2 + \xi_2) - \partial_2 f(a) )
	+ h_1 (\partial_1 f(a_1 + \xi_1, a_2) - \partial_1 f(a) ).
    \end{equation*}
    Clearly
    \begin{equation*}
      \frac{\abs{e(h)}}{\abs{h}}
	\leq
	  \abs{\partial_2 f(a_1 + h_1, a_2 + \xi_2) - \partial_2 f(a) }
	  + \abs{\partial_1 f(a_1 + \xi_1, a_2) - \partial_1 f(a) },
    \end{equation*}
    which converges to $0$ as $h \to 0$.
  \end{proof}

  Note, however, it is possible for a function to be differentiable, and for the partial derivatives to exist and be discontinuous.
  \begin{example}
    Let $f \colon \R^2 \to \R$ be defined by $f(x) = \abs{x}^2 \sin( 1/ \abs{x})$ when $x \neq 0$, and $f(0) = 0$.
    Then $f$ is differentiable on all of $\R^2$ (including $x = 0$), and hence all partial derivatives of $f$ exist at all points in $\R^2$.
    However, $\partial_1 f$ and $\partial_2 f$ are not continuous at $x = 0$.
  \end{example}
  

  \begin{definition}
    Let $U \subset \R^m$ be a domain, and $a \in U$.
    We say a function $U \to \R^n$ is differentiable if if there exists a linear transformation $T: \R^m \to \R^n$ and a function $e$ such that
    \begin{enumerate}
      \item
	$f(a + h) = f(a) + Th + e(h)$

      \item
	\emph{and} $\lim_{h \to 0} \abs{ e(h) } / \abs{h} = 0$.
    \end{enumerate}
  \end{definition}

  Note this is almost \emph{the same} as Definition~\ref{dfnDf}.
  The only change is that the linear transformation $T$ is now a map from $\R^m$ to $\R^n$ instead.

  \begin{proposition}
    Let $f = (f_1, \dots, f_n) \colon \R^m \to \R^n$ and $a \in \R^m$.
    The function $f$ is differentiable at $a$ if and only if each coordinate function $f_i$ is differentiable at $a$.
    Further, the derivative $Df$ is a $n \times m$ matrix given by
    \begin{equation*}
      Df_a = \begin{pmatrix}
	\partial_1 f_1(a) & \partial_2 f_1(a) & \cdots & \partial_m f_1( a )\\
	\partial_1 f_2(a) & \partial_2 f_2(a) & \cdots & \partial_m f_2( a )\\
	\vdots & \vdots & & \vdots\\
	\partial_1 f_n(a) & \partial_2 f_n(a) & \cdots & \partial_m f_n( a )\,.
      \end{pmatrix}
    \end{equation*}
  \end{proposition}

  As before, the derivative $Df$ is also called the \emph{Jacobian Matrix}.


  \section{Tangent planes and Level Sets}

  Let $f:\R^d \to \R^n$ be differentiable.

  \begin{definition}
    The \emph{graph} of $f$ is the set $\Gamma \subset \R^d \times \R^n$ defined by
    \begin{equation*}
      \Gamma = \set{ (x, f(x)) \st x \in \R^d }.
    \end{equation*}
    Given a point $(a, f(a)) \in \Gamma$ we define the \emph{tangent plane} of $f$ at the point $a$ by the equation
    \begin{equation*}
      y = f(a) + Df_a( x - a)
    \end{equation*}
  \end{definition}

  Note that the tangent plane is a $d$-dimensional hyper-plane in $\R^d \times \R^n$.
  It is the \emph{best linear approximation} to the graph $\Gamma$ at the point $a$.
  Projecting the tangent plane into 2 dimensions (by freezing other coordinates) gives you a tangent line.

  \begin{definition}
    The \emph{tangent space} to the graph~$\Gamma$ at the point $(a, f(a))$, denoted by $T \Gamma_{(a,f(a))}$ is defined by
    \begin{equation*}
      T\Gamma_{(a, f(a))} = \set{ (x, y) \st y = Df_a x,\ x \in \R^d }\,.
    \end{equation*}
  \end{definition}
  Namely, the tangent space is the space of all vectors parallel to the tangent plane, and passing through the origin.

  \begin{definition}
    Given $c \in \R$ we define the \emph{level set of $f$} to be the set $\set{ x \in \R^d \st f(x) = c}$.
  \end{definition}

  If $d = 2$, then level sets are typically curves.
  If $d = 3$, then level sets are typically surfaces.
  In higher dimensions (for ``nice functions'') level sets of $f$ are typically $d-1$-dimensional hyper-surfaces.

  \begin{example}
    Let $d = 3$ and $f(x) = \abs{x}^2$.
    Then $\set{f(x) = c}$ is the sphere of radius $\sqrt{c}$ for $c > 0$, a point for $c = 0$ and the empty set for $c < 0$.
  \end{example}

  Level sets are very useful in plotting, and are often used to produce \emph{contour plots}.
  We will see later that if $v$ is tangent to a level set of $f$, then $D_v f = 0$.
  Moreover if $f \colon \R^d \to \R$, then $\grad f$ (defined to be $(Df)^T$) is orthogonal to level sets.

  \section{Chain rule}

  The one variable calculus rules for differentiation of sums, products and quotients (when they make sense) are still valid in higher dimensions.
  \begin{proposition}
    Let $f, g: \R^d \to \R$ be two differentiable functions.
    \begin{itemize}
      \item
	$f + g$ is differentiable and $D(f + g) = Df + Dg$.
      \item
	$fg$ is differentiable and $D(fg) = f Dg + g Df$.
      \item
	At points where $g \neq 0$, $f/g$ is also differentiable and
	\begin{equation*}
	  D\paren[\Big]{ \frac{f}{g} }
	    = \frac{g Df - f Dg}{g^2}
	\end{equation*}
    \end{itemize}
  \end{proposition}

  These follow in a manner very similar to the one variable analogues, and are left for you to verify.
  The one rule that is a little different in this context is the differentiation of composites.

  \begin{theorem}[Chain Rule]
    Let $U \subseteq \R^m$, $V \subseteq \R^n$ be domains, $g:U \to V$, $f:V \to \R^d$ be two differentiable functions.
    Then $f \circ g:U \to \R^d$ is also differentiable and
    \begin{equation*}
      D(f \circ g)_a = (D f_{g(a)}) (Dg_a)
    \end{equation*}
  \end{theorem}
  Note $Df_g$ and $Dg$ are both matrices, and the product above is the \emph{matrix product} of $Df$ and $Dg$.
  \begin{proof}
    The basic intuition is as follows.
    Since $f, g$ are differentiable we know there exist functions $e_1$ and $e_2$ such that 
    \begin{equation*}
      g(a + h) = g(a) + Dg_a + e_2(h)
      \quad\text{and}\quad
      f( g(a) + h) = f(g(a)) + Df_{g(a)} + e_1(h)\,,
    \end{equation*}
    with $\lim_{h \to 0} e_i(h) / \abs{h} = 0$.
    Consequently,
    \begin{align*}
      f(g(a + h))
	&= f\paren[\big]{ g(a) + Dg_a h + e_2(h)}
      \\
	&= f( g( a) )
	  + Df_{g(a)} \paren[\big]{ D g_a h + e_2(h)}
	  + e_1 \paren[\big]{ D g_a h + e_2(h)}
      \\
	&= f(g(a)) + \paren[\big]{Df_{g(a)} \, Dg_a} h
	  + e_3(h)\,,
    \end{align*}
    where 
    \begin{equation*}
      e_3(h) = Df_{g(a)} e_2(h) + e_1 \paren[\big]{ D g_a h + e_2(h)}\,.
    \end{equation*}
    Now to finish the proof one only needs to show $\lim_{h \to 0} e_3(h) / \abs{h} = 0$, which can be done directly from the $\epsilon$-$\delta$ definition.
  \end{proof}

  \begin{remark}
  An extremely useful arises when $d = 1$ and we need to compute $\partial_i (f \circ g)$.
  In this case, by the chain rule
  \begin{equation*}
    \partial_i (f \circ g) = (Df_g) (Dg) e_i = \sum_{j = 1}^n \partial_j f \Bigr|_{g} \partial_i g_j \,.
  \end{equation*}
  While this can be derived immediately by multiplying the matrices $Df_g$ and $Dg$, it arises often enough that it is worth directly remembering. 
  \end{remark}

  Another version of the chain rule that often shows up in problems is as follows.
  \begin{proposition}
    Suppose $z$ is a function of $x$ and $y$, and $x$ and $y$ are in turn functions of $s$ and $t$.
    Then
    \begin{equation}\label{e:chain}
      \partial_s z = \partial_x z \partial_s x + \partial_y z \partial_s y\,,
      \qquad\text{and}\qquad
      \partial_t z = \partial_x z \partial_t x + \partial_y z \partial_t y\,.
    \end{equation}
  \end{proposition}
  \begin{proof}
    Let $z = f(x, y)$, $x = g(s, t)$ and $y = h(s, t)$ for some functions $f, g, h$.
    Define $\psi\colon \R^2 \to \R^2$ by $\psi = (g, h)$.
    Now equation~\eqref{e:chain} follows immediately by realizing $z = f \circ \psi$ and using the chain rule.
  \end{proof}

  \begin{example}
    Compute $\frac{d}{dx} x^x$ and $\frac{d}{dt} \int_0^t e^{-(t - s)^2} \, ds$.
  \end{example}

  \begin{proposition}
    Let $f, g \colon \R^d \to \R$ be differentiable. Then $fg \colon \R^d \to \R$ is differentiable and $D(fg) = f (Dg) + g (Df)$.
  \end{proposition}
  \begin{proof}
    Let $F(x, y) = xy$ and $G(x) = (f(x), g(x))$.
    Then observe $f g = F \circ G$, and the conclusion follows from the chain rule.
  \end{proof}
  \begin{remark}
    A similar trick can be used to prove the quotient rule.
  \end{remark}

  As a consequence, here is a ``proof'' that directional derivatives in directions tangent to level sets vanish.
  \begin{proposition}
    Let $\Gamma = \set{ x \st f(x) = c}$ be a level set of a differentiable function $f$.
    Let $\gamma:[-1, 1] \to \Gamma$ be a differentiable function, $v = D\gamma (0)$, and $a = \gamma(0)$.
    Then $D_vf( a ) = 0$.
  \end{proposition}
  
  Think of $\gamma(t)$ as the position of a particle at time $t$.
  If for all $t$, $\gamma(t)$ belongs to the curve $\Gamma$, then the velocity $D\gamma$ should be tangent to the curve $\gamma$, and thus thus the vector $v$ above should be tangent to $\Gamma$. (When we can define this rigorously, we will revisit it and prove it.)

  \begin{proof}
    Note $f \circ \gamma  = c$ (since $\gamma(t) \in \Gamma$ for all $t$).
    By the chain rule $D( f \circ \gamma ) = D f_{\gamma} D \gamma$.
    At $t = 0$ this gives $Df_{\gamma(0)} v = 0 \implies D_v f(\gamma(0)) = 0$ as desired.
  \end{proof}

  \begin{definition}
    If $f:\R^d \to R$ is differentiable, define the \emph{gradient} of $f$ (denoted by $\grad f$) to be the transpose of the derivative of $f$.
  \end{definition}

  We've seen above that if $v$ is tangent to a level set of $f$ at $a$, then $D_v f(a) = 0$.
  This is equivalent to saying $\grad f(a) \cdot v = 0$, or that \emph{the gradient of $f$ is perpendicular to level sets of $f$.}
  Note, in directions tangent to level sets, $f$ is changing the \emph{least}.
  One would expect that in the perpendicular direction (given by $\grad f$), the function $f$ is changing the most.
  This is shown by the following proposition.
  \begin{proposition}
    If $v \in \R^d$ with $\abs{v} = 1$.
    Then $D_v f(a)$ is maximised when $v = \grad f(a)$ and $D_v f(a)$ is minimised when $v = - \grad f(a)$.
  \end{proposition}

  \begin{remark}
    This fact is often used when numerically finding minima of functions, and is known as \emph{the method of gradient descent}.
    Namely, start with a guess for the minimum $x_0$.
    Now choose successive approximations to move directly against $\grad f$.
    That is, define
    \begin{equation*}
      x_{n+1} = x_n + \gamma_n \grad f(x_n)\,,
    \end{equation*}
    for some small $\gamma_n$.
    Usually, the standard numerical algorithms suggest using
    \begin{equation*}
      \gamma_n = \frac{ (x_n - x_{n-1}) \cdot ( \grad f(x_n) - \grad f(x_{n-1}) ) }{ \abs{ \grad f(x_n) - \grad f(x_{n-1})  }^2 }\,,
    \end{equation*}
    which guarantees converge to a local minimum, under certain assumptions on $f$.
  \end{remark}

  \section{Higher order derivatives}

  Given a function $f$, treat $\partial_i f$ as a function.
  If $\partial_i f$ is itself a differentiable function, we can differentiate it again.
  The second derivative (denoted by $\partial_j \partial_i f$) is called a second order partial of $f$.
  These can further be differentiated to obtain third order partials.

  \begin{theorem}[Clairaut]
    If $\partial_i \partial_j f$ and $\partial_j \partial_i f$ both exist in a neighbourhood of $a$, and are continuous at $a$ then they must be equal.
  \end{theorem}

  If the mixed second order partials are not continuous, however, they need not be equal.
  \begin{example}
    Let $f(x, y) = x^3 y / (x^2 + y^2)$ for $(x, y) \neq 0$ and $f(0, 0) = 0$. Then $\partial_x \partial_y f(0, 0) = 1$ but $\partial_y \partial_x f(0, 0) = 0$.
  \end{example}

  \begin{proof}[Proof of Clairaut's theorem]
    Here's the idea in 2D (the same works in higher dimensions).
    For simplicity assume $a = 0$.
    \begin{itemize}
      \item
	Let $R$ be the rectangle with corners $(0, 0)$, $(h, 0)$, $(0, k)$, $(h, k)$.

      \item
	Using the mean value theorem, show $f(h, k) - f(h, 0) - f(0, k) + f(0, 0) = h k \partial_x \partial_y f(\alpha)$ for some point $\alpha \in R$.

      \item
	Observe $f(h, k) - f(h, 0) - f(0, k) + f(0, 0) = f(h, k) - f(0, k) - f(h, 0) + f(0, 0)$ and so using the mean value theorem show $f(h, k) - f(h, 0) - f(0, k) + f(0, 0) = h k \partial_y \partial_x f(\beta)$ for some point $\beta \in R$.

      \item
	Note that as $(h, k) \to 0$, we have $\alpha, \beta \to 0$.
	Consequently, if $\partial_x \partial_y f$ and $\partial_y \partial_x f$ are both continuous at $0$ we must have
	\begin{equation*}
	  \partial_x \partial_y f(0,0)
	    = \lim_{(h, k) \to 0} \frac{f(h, k) - f(h, 0) - f(0, k) + f(0, 0)}{hk}
	    = \partial_y \partial_x f(0,0),
	\end{equation*}
	proving equality as desired.
	\qedhere
    \end{itemize}
  \end{proof}

  \begin{definition}
    A function is said to be of class $C^k$ if all its $k^\text{th}$-order partial derivatives exist and are continuous.
  \end{definition}

  By Clairaut's theorem, we know that mixed partials are equal for $C^k$ functions.

  \section{Maxima and Minima}

  \begin{definition}
    A function $f$ has a local maximum at $a$ if $\exists \epsilon > 0$ such that whenever $\abs{x - a} < \epsilon$ we have $f(x) \leq f(a)$.
  \end{definition}

  Our aim is now to understand what having a local maximum / minimum translates to in terms of derivatives of $f$.
  For this we do a simple calculation: Observe that if $f$ has a local maximum at $a$, then for all $v \in \R^d - \set{0}$ the function $f( a + tv )$ must have a local maximum at $t = 0$.
  Hence we must have $\partial_t f( a + tv )|_{t = 0} = 0$ and $\partial_t^2 f( a + tv )|_{t = 0} \leq 0$.
  Using the chain rule, we compute
  \begin{equation*}
    \partial_t f( a + tv ) = \sum_{i = 1}^d \partial_i f( a + tv ) v_i
    \quad\text{and}\quad
    \partial_t^2 f( a + tv ) = \sum_{i,j = 1}^d \partial_i \partial_j f( a + tv ) v_i v_j
  \end{equation*}
  Thus at a local maximum we must have
  \begin{equation*}
    \sum_{i = 1}^d \partial_i f( a ) v_i = 0
    \quad\text{and}\quad
    \sum_{i,j = 1}^d \partial_i \partial_j f( a ) v_i v_j \leq 0 
  \end{equation*}
  for every $v \in \R^d$.
  This translates to the following proposition.

  \begin{proposition}
    If $f$ is a $C^2$ function which has a local maximum at $a$, then
    \begin{enumerate}
      \item
	The first derivative $Df$ must vanish at $a$ (i.e.\ $Df_a = 0$).
	$Df_a = 0$

      \item
	The \emph{Hessian} $Hf$ is negative semi-definite at $a$.
    \end{enumerate}

    For a local minimum, we replace \emph{negative semi-definite} above with \emph{positive semi-definite}.
  \end{proposition}

  \begin{definition}
    The \emph{Hessian} of a $C^2$ function (denoted by $Hf$) is defined to be the matrix
    \begin{equation*}
      Hf = \begin{pmatrix}
	\partial_1 \partial_1 f & \partial_2 \partial_1 f & \cdots & \partial_d \partial_1 f\\
	\partial_1 \partial_2 f & \partial_2 \partial_2 f & \cdots & \partial_d \partial_2 f\\
	\vdots & \vdots & & \vdots\\
	\partial_1 \partial_d f & \partial_2 \partial_d f & \cdots & \partial_d \partial_d f
      \end{pmatrix}
    \end{equation*}
  \end{definition}

  Note if $f \in C^2$, $Hf$ is symmetric.

  \begin{definition}
    Let $A$ be a $d \times d$ \emph{symmetric} matrix.
    \begin{itemize}
      \item
	If $(Av) \cdot v \leq 0$ for all $v \in \R^d$, then $A$ is called \emph{negative semi-definite}.

      \item
	If $(Av) \cdot v < 0$ for all $v \in \R^d$, then $A$ is called \emph{negative definite}.

      \item
	If $(Av) \cdot v \geq 0$ for all $v \in \R^d$, then $A$ is called \emph{positive semi-definite}.

      \item
	If $(Av) \cdot v > 0$ for all $v \in \R^d$, then $A$ is called \emph{positive definite}.
    \end{itemize}
  \end{definition}

  Recall a symmetric matrix is positive semi-definite if all the eigenvalues are non-negative.
  In 2D this simplifies to the following:
  \begin{proposition}
    Let $A$ be the symmetric $2 \times 2$ matrix $(\begin{smallmatrix} a & b\\ b & c\end{smallmatrix})$.
    \begin{enumerate}
      \item 
	$A$ is positive definite if and only if $a > 0$ and $ac - b^2 > 0$.
      \item 
	$A$ is negative definite if and only if $a < 0$ and $ac - b^2 > 0$.
      \item
	$A$ is positive semi-definite if and only if $a, c \geq 0$ and $ac - b^2 \geq 0$.
      \item 
	$A$ is negative semi-definite if and only if $a, c \leq 0$ and $ac - b^2 \geq 0$.
    \end{enumerate}
  \end{proposition}


  Finally, we address the converse: Namely, we look for a condition on the derivatives of $f$ that guarantees that $f$ attains a local maximum or minimum at $a$.
  \begin{theorem}\label{thmMaxCriterion}
    Let $f$ be a $C^2$ function.
    \begin{enumerate}
      \item If $Df_a = 0$ and further $Hf_a$ is positive definite, then $f$ attains a local minimum at $a$.
      \item If $Df_a = 0$ and further $Hf_a$ is negative definite, then $f$ attains a local maximum at $a$.
    \end{enumerate}
  \end{theorem}

  The proof uses Taylor's theorem, and we will prove it in Section~\ref{s:taylor}, below

  \begin{definition}
    We say $a$ is a \emph{local saddle} of $f$ if there exist two linearly independent vectors $v_1$ and $v_2$ such that $f$ has a strict local minimum in direction $v_1$ and a strict local maximum in direction $v_2$.
  \end{definition}

  \begin{proposition}
    If $f$ is $C^2$, $Df_a = 0$ and $Hf_a$ has at least one strictly positive and one strictly negative eigenvalue, then $a$ is a local saddle of $f$.
  \end{proposition}

  This corresponds to points where $f$ has a local maximum in one direction and a local minimum in the other.

  \begin{example}
    The function $\abs{x}^2$ has a local minimum at $0$.
    The function $-\abs{x}^2$ has a local maximum at $0$.
    The function $x_1^2 - x_2^2$ has a saddle at $0$.
  \end{example}

  \begin{example}
    Let $f \colon \R^d \to \R$, and let $\Gamma \subseteq \R^{d+1}$ be the graph of $f$ (i.e.\ $\Gamma = \set{ (x, y) \st x \in \R^d,\ y = f(x)}$).
    Fix $(x', y') \in \R^{d+1}$, and let $(a, f(a))$ be the point on $\Gamma$ which is closest to $(x', y')$.
    Then $x' - a$ is parallel to $\grad f(a)$ and $(x' - a, y' - f(a))$ is normal to the tangent plane at $(a, f(a))$.
  \end{example}
  \begin{proof}
    Let $d(x) = \abs{x -x'}^2 + (f(x) - y')^2$.
    At a max $\grad d = 0$, and hence
    \begin{equation}\label{e:aminusxprime}
      2(a - x') + 2(f(a) - y') \grad f(a) = 0\,.
    \end{equation}
    This shows $x' - a$ is parallel to $\grad f(a)$.

    For the second assertion recall that the tangent plane is defined by
    \begin{equation*}
      y = f(a) + \grad f(a) \cdot (x - a) \,.
    \end{equation*}
    This is equivalent to saying $(\grad f(a), -1) \cdot (x - a, y-f(a)) = 0$, and hence $(\grad f(a), -1)$ is normal to the tangent plane.
    But from~\eqref{e:aminusxprime} we immediately see
    \begin{equation*}
      \colvec{x' - a \\ y' - f(a)} = -(y' - f(a)) \colvec{\grad f(a)\\-1}\,,
    \end{equation*}
    and hence $(x' - a, y' - f(a))$ is also normal to the tangent plane at $a$.
  \end{proof}


  \section{Taylors theorem}\label{s:taylor}
  
  \begin{theorem}
    If $f \in C^2$, then 
    \begin{equation}\label{eqnTaylor2}
      f(a + h) = f(a) + Df_a h + \frac{1}{2} h \cdot Hf_a h + R_2(h),
    \end{equation}
    where $R_2(h)$  is some function such that
    \begin{equation*}
      \lim_{h \to 0} \frac{R_2(h)}{\abs{h}^2} \to 0.
    \end{equation*}
  \end{theorem}

  In coordinates equation~\eqref{eqnTaylor2} is
  \begin{equation*}
    f(a + h) = f(a) + \sum_i \partial_i f(a) h_i + \frac{1}{2} \sum_{i,j} \partial_i \partial_j f(a) h_i h_j + R_2(h).
  \end{equation*}

  \begin{proof}
    Let $g(t) = f(a + th)$.
    Using the 1D Taylors theorem we have
    \begin{equation*}
      g(1) = g(0) + g'(0) + \frac{1}{2} g''(\xi)
    \end{equation*}
    for some $\xi \in (0, 1)$. Writing this in terms of $f$ finishes the proof.
  \end{proof}

  The same technique can show the following mean value theorem:
  \begin{theorem}[Mean value theorem]
    If $f$ is differentiable on the entire line joining $a$ and $b$,
    \begin{equation*}
      f(b) = f(a) + (b - a) \cdot \grad f(\xi)
    \end{equation*}
    for some point $\xi$ on the line segment joining $a$ and $b$.
  \end{theorem}

  Taylor's theorem allows us to prove Theorem~\ref{thmMaxCriterion}.
  In order to do this, we need a standard lemma from linear algebra.
  \begin{lemma}
    If $A$ is a $d \times d$ positive definite symmetric matrix, then for every $v \in \R^d$ we have $(Av) \cdot v \geq \lambda_0 \abs{v}^2$, where $\lambda_0$ is the smallest eigenvalue of $A$.
  \end{lemma}
  \begin{proof}
    By the spectral theorem, we know there exists an orthonormal basis of $\R^d$ consisting of eigenvectors of $A$.
    Let $\set{e_1, \dots, e_d}$ denote this basis, and let $\lambda_i$ denote the corresponding eigenvalue (i.e.\ $A e_i = \lambda_i e_i$).
    Now let $v_i = v \cdot e_i$, and observe $v = \sum v_i e_i$ and hence $Av = \sum \lambda_i v_i e_i$.
    Thus
    \begin{align*}
      (Av) \cdot v &= \paren[\Big]{\sum_i \lambda_i v_i e_i}
	\cdot \paren[\Big]{ \sum_j v_j e_j}
	= \sum_{i,j} \lambda_i v_i v_j e_i \cdot e_j
	= \sum_i \lambda_i v_i^2
	\\
	&\geq (\min_i \lambda_i) \sum_i  v_i^2 
	= \lambda_0 \abs{v}^2\,.
	\qedhere
    \end{align*}
  \end{proof}

  \begin{proof}[Proof of Theorem~\ref{thmMaxCriterion}]
    Suppose $Df_a = 0$ and $Hf_a$ is positive definite.
    Let $\lambda_0$ be the smallest eigenvalue of $Hf_a$.
    Expanding in terms of an orthonormal basis of eigenfunctions of $Hf_a$ we see $Hh \cdot h \geq \lambda_0 \abs{h}^2$.

    Now choose $\delta > 0$ so that $\abs{R_2(h)} < \lambda_0 \abs{h}^2 / 2$ for $h < \delta$, and note $f(a + h) \geq f(a) + \frac{\abs{h}^2}{2} \geq f(a)$, showing $f$ has a local min at $a$.
  \end{proof}

  A higher order version of Taylor's theorem is also true.
  It is usually stated using the multi-index notation, collecting all mixed partials that are equal.

  \begin{definition}
    Let $\alpha = ( \alpha_1, \alpha_2, \dots, \alpha_d)$, with $\alpha_i \in \N \cup \set{0}$.
    If $h \in \R^d$ define 
    \begin{equation*}
      h^\alpha = h_1^{\alpha_1} h_2^{\alpha_2} \cdots h_d^{\alpha_d},
      \quad
      \abs{\alpha} = \alpha_1 + \cdots + \alpha_d,
      \quad\text{and}\quad
      \alpha! = \alpha_1! \, \alpha_2!\, \cdots \alpha_d!.
    \end{equation*}
    Given a $C^{\abs{\alpha}}$ function $f$, define
    \begin{equation*}
      D^\alpha f = \partial_1^{\alpha_1} \partial_2^{\alpha_2} \cdots \partial_d^{\alpha_d} f,
    \end{equation*}
    with the convention that $\partial_i^0 f = f$.
  \end{definition}

  \begin{theorem}
    If $f$ is a $C^n$ function on $\R^d$ and $a \in \R^d$ we have
    \begin{equation*}
      f(a + h) = \sum_{\abs{\alpha} < n} \frac{1}{\alpha!} D^\alpha f(a) + R_n(h),
    \end{equation*}
    for some function $R_n$ such that
    \begin{equation*}
      \lim_{h \to 0} \frac{R_n(h)}{\abs{h}^n} = 0.
    \end{equation*}
  \end{theorem}

  The proof follows from the one variable Taylor's theorem in exactly the same as our second order version does, and collecting all mixed partials that are equal puts it in the above form.
\end{document}
