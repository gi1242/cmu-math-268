#! /bin/bash

# Generate two column files
sed -e 's:\(\\usepackage\).*\({gimac}\):\1[twocolumn]\2:' \
    -i preamble.tex
precompile.sh
latexmk -g ch*-*.tex notes.tex 
cp -uLv notes.pdf ch*-*.pdf WWW/pdfs

# Generate tablet version
sed -e 's:\(\\usepackage\[\).*\(\]{gimac}\):\1tablet\2:' \
    -i preamble.tex
precompile.sh
latexmk -g notes.tex
cp -uLv notes.pdf WWW/pdfs/notes-tablet.pdf

# Revert to draft version for me
sed -e 's:\(\\usepackage\[\).*\(\]{gimac}\):\1draft\2:' \
    -i preamble.tex
precompile.sh
