% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{3}\fi
  \chapter{Multiple Integrals}%endchapter

  \section{Double Integrals}

  Let $R = [a, b] \times [c, d] \subseteq \R^2$ be a rectangle, and $f:R \to \R$ be continuous.
  Let $P = \set{ x_0, \dots, x_M, y_0, \dots, y_M}$ where $a = x_0 < x_1 < \cdots < x_M = b$ and $c = y_0 < y_1 < \cdots < y_M = d$.
  The set $P$ determines a partition of $R$ into a grid of (non-overlapping) rectangles $R_{i,j} = [x_i, x_{i+1}] \times [y_j, y_{j+1}]$ for $0 \leq i < M$ and $0 \leq j < N$.
  Given $P$, choose a collection of points $\Xi = \set{\xi_{i,j}}$ so that $\xi_{i,j} \in R_{i,j}$ for all $i, j$.
  \begin{definition}
    The \emph{Riemann sum} of $f$ with respect to the partition $P$ and points $\Xi$ is defined by
    \begin{equation*}
      \mathcal R(f, P, \Xi)
	\defeq \sum_{i=0}^{M-1} \sum_{j = 0}^{N-1}
	  f(\xi_{i, j}) \area(R_{i,j})
	= \sum_{i=0}^{M-1} \sum_{j = 0}^{N-1}
	  f(\xi_{i, j}) (x_{i+1} - x_i) (y_{j+1} - y_j)
    \end{equation*}
  \end{definition}

  \begin{definition}
    The \emph{mesh size} of a partition $P$ is defined by
    \begin{equation*}
      \norm{P} = \max \set{ x_{i+1} - x_i \st 0 \leq i < M} \cup \set{y_{j+1} - y_j \st 0 \leq j \leq N }.
    \end{equation*}
  \end{definition}

  \begin{definition}
    The \emph{Riemann integral} of $f$ over the rectangle $R$ is defined by
    \begin{equation*}
      \int_R f(x, y) \, dx \, dy
      \defeq
      \lim_{\norm{P} \to 0}
	\mathcal R( f, P, \Xi ),
    \end{equation*}
    provided the limit exists and is independent of the choice of the points $\Xi$.
    A function is said to be \emph{Riemann integrable} over $R$ if the Riemann integral exists and is finite.
  \end{definition}

  \begin{remark}
    A few other popular notation conventions used to denote the integral are
    \begin{equation*}
      \iint_R f \, dA,
      \quad
      \iint_R f \, dx \, dy,
      \quad
      \iint_R f \, dx_1 \, dx_2,
      \quad\text{and}\quad
      \iint_R f.
    \end{equation*}
  \end{remark}

  \begin{remark}
    The double integral represents the volume of the region under the graph of $f$.
    Alternately, if $f(x, y)$ is the density of a planar body at point $(x, y)$, the double integral is the total mass.
  \end{remark}

  \begin{theorem}
    Any bounded continuous function is Riemann integrable on a bounded rectangle.
  \end{theorem}

  \begin{remark}
    Most bounded functions we will encounter will be Riemann integrable.
    Bounded functions with reasonable discontinuities (e.g. finitely many jumps) are usually Riemann integrable on bounded rectangle.
    An example of a ``badly discontinuous'' function that is not Riemann integrable is the function $f(x, y) = 1$ if $x, y \in \Q$ and $0$ otherwise.
    %s \emph{provided} they are bounded or don't grow ``too fast'' near their singularities.
  \end{remark}



  Now suppose $U \subseteq \R^2$ is an nice bounded%
  \footnote{%
    We will subsequently always assume $U$ is ``nice''. Namely, $U$ is open, connected and the boundary of $U$ is a piecewise differentiable curve.
    More precisely, we need to assume that the ``area'' occupied by the boundary of $U$ is $0$.
    While you might suspect this should be true for all open sets, it isn't!
    There exist open sets of \emph{finite area} whose boundary occupies an infinite area!}
  domain, and $f:U \to \R$ is a function.
  Find a bounded rectangle $R \supseteq U$, and as before let $P$ be a partition of $R$ into a grid of rectangles.
  Now we define the Riemann sum by only summing over all rectangles $R_{i,j}$ that are completely contained inside $U$.
  Explicitly, let
  \begin{equation*}
    \chi_{i,j}
      = \begin{cases}
	  1 & R_{i,j} \subseteq U\\
	  0 & \text{otherwise}.
	\end{cases}
  \end{equation*}
  and define
  \begin{equation*}
    \mathcal R(f, P, \Xi, U)
      %\defeq \sum_{i=0}^{M-1} \sum_{j = 0}^{N-1}
      %  \chi_{i,j} f(\xi_{i, j}) \area(R_{i,j})
      \defeq \sum_{i=0}^{M-1} \sum_{j = 0}^{N-1} \chi_{i,j}
	f(\xi_{i, j}) (x_{i+1} - x_i) (y_{j+1} - y_j).
  \end{equation*}
  \begin{definition}
    The \emph{Riemann integral} of $f$ over the \emph{domain $U$} is defined by
    \begin{equation*}
      \int_U f(x, y) \, dx \, dy
      \defeq
      \lim_{\norm{P} \to 0}
	\mathcal R( f, P, \Xi, U ),
    \end{equation*}
    provided the limit exists and is independent of the choice of the points $\Xi$.
    A function is said to be \emph{Riemann integrable} over $R$ if the Riemann integral exists and is finite.
  \end{definition}

  \begin{theorem}
    Any bounded continuous function is Riemann integrable on a bounded region.
  \end{theorem}
  \begin{remark}
    As before, most reasonable \emph{bounded} functions we will encounter will be Riemann integrable.
  \end{remark}

  To deal with unbounded functions over unbounded domains, we use a limiting process.
  \begin{definition}
    Let $U \subseteq \R^2$ be a domain (which is not necessarily bounded) and $f:U \to \R$ be a (not necessarily bounded) function.
    We say $f$ is integrable if
    \begin{equation*}
      \lim_{R \to \infty} \int_{U \cap B(0, R)} \abs{f} \, dA
    \end{equation*}
    exists and is finite.
  \end{definition}
  \begin{proposition}
    If $f$ is integrable on the domain $U$, then
    \begin{equation*}
      \lim_{R \to \infty} \int_{U \cap B(0, R)} f \, dA
    \end{equation*}
    exists and is finite.
  \end{proposition}
  \begin{remark}
    If $f$ is integrable, then the above limit is independent of how you expand your domain.
    Namely, you can take the limit of the integral over $U \cap [-R, R]^2$ instead, and you will still get the same answer.
  \end{remark}
  \begin{definition}
    If $f$ is integrable we define
    \begin{equation*}
      \int_U f \, dx \, dy = \lim_{R \to \infty} \int_{U \cap B(0, R)} f \, dA
    \end{equation*}
  \end{definition}
  \section{Iterated integrals and Fubini's theorem}

  Let $U \subseteq \R^2$ be a domain.
  \begin{definition}
    For $x \in \R$, define
    \begin{equation*}
      S_x U = \set{y \st (x, y) \in U}
      \quad\text{and}\quad
      T_y U = \set{x \st (x, y) \in U}
    \end{equation*}
  \end{definition}

  \begin{example}
    If $U = [a, b] \times [c, d]$ then
    \begin{equation*}
      S_x U = 
      \begin{cases}
	[c,d] & x \in [a, b]\\
	\emptyset & x \not\in [a, b]
      \end{cases}
      \quad\text{and}\quad
      T_y U = \begin{cases}
	[a, b] & y \in [c, d]\\
	\emptyset & y \not\in [c,d].
      \end{cases}
    \end{equation*}
  \end{example}

  For domains we will consider, $S_x U$ and $T_y U$ will typically be an interval (or a finite union of intervals).
  \begin{definition}
    Given a function $f:U \to \R$, we define the two \emph{iterated} integrals by
    \begin{equation*}
      \int_{x \in \R} \paren[\Big]{ \int_{y \in S_xU} f(x, y) \, dy } \, dx
      \quad\text{and}\quad
      \int_{y \in \R} \paren[\Big]{ \int_{x \in T_y U} f(x, y) \, dx } \, dy,
    \end{equation*}
    with the convention that an integral over the empty set is $0$.
    (We included the parenthesis above for clarity; and will drop them as we become more familiar with iterated integrals.)
  \end{definition}

  Suppose $f(x, y)$ represents the density of a planar body at point $(x, y)$.
  For any $x \in \R$,
  \begin{equation*}
    \int_{y \in S_xU} f(x, y) \, dy
  \end{equation*}
  represents the mass of the body contained in the vertical line through the point $(x, 0)$.
  It's only natural to expect that if we integrate this with respect to $y$, we will get the total mass, which is the double integral.
  By the same argument, we should get the same answer if we had sliced it horizontally first and then vertically.
  Consequently, we expect both iterated integrals to be equal to the double integral.
  This is true, under a finiteness assumption.

  \begin{theorem}[Fubini's theorem]
    Suppose $f:U \to \R$ is a function such that either
    \begin{equation}\label{eqnFubiniFiniteness}
      \int_{x \in \R} \paren[\Big]{ \int_{y \in S_xU} \abs{f(x, y)} \, dy } \, dx < \infty
      \quad\text{or}\quad
      \int_{y \in \R} \paren[\Big]{ \int_{x \in T_y U} \abs{f(x, y)} \, dx } \, dy < \infty,
    \end{equation}
    then $f$ is integrable over $U$ and
    \begin{equation*}
      \int_U f \, dA
	= \int_{x \in \R} \paren[\Big]{ \int_{y \in S_xU} f(x, y) \, dy } \, dx
	= \int_{y \in \R} \paren[\Big]{ \int_{x \in T_y U} f(x, y) \, dx } \, dy.
    \end{equation*}
  \end{theorem}

  Without the assumption~\eqref{eqnFubiniFiniteness} the iterated integrals need not be equal, even though both may exist and be finite.
  \begin{example}
    Define
    \begin{equation*}
      f(x, y)
	= -\partial_x \partial_y \tan^{-1}\paren[\big]{\frac{y}{x}}
	= \frac{x^2 - y^2}{(x^2 + y^2)^2}.
    \end{equation*}
    Then
    \begin{equation*}
      \int_{x = 0}^1 \int_{y = 0}^1 f(x, y) \, dy \, dx = \frac{\pi}{4}
      \quad\text{and}\quad
      \int_{y = 0}^1 \int_{x = 0}^1 f(x, y) \, dx \, dy = -\frac{\pi}{4}
    \end{equation*}
  \end{example}
  \begin{example}
    Let $f(x, y) = (x - y) / (x + y)^3$ if $x, y > 0$ and $0$ otherwise, and $U = (0, 1)^2$.
    The iterated integrals of $f$ over $U$ both exist, but are not equal.
  \end{example}

  \begin{example}
    Define
    \begin{equation*}
      f(x, y) = \begin{cases}
	1 & y \in (x, x+1) \text{ and } x \geq 0\\
	-1 & y \in (x-1, x) \text{ and } x \geq 0\\
	0 & \text{otherwise}.
      \end{cases}
    \end{equation*}
    Then the iterated integrals of $f$ both exist and are not equal.
  \end{example}
  \begin{example}
    Compute the area of a parallelogram.
  \end{example}
  \begin{example}
    Let $U$ be the triangle with vertices $(0, 0)$, $(1, 1)$, and $(0, 1)$ and $f(x, y) = e^{-y^2}$.
    Compute $\int_U f \, dA$.
  \end{example}

  \section{Triple integrals}

  Triple integrals are just like double integrals, except we integrate over regions in $\R^3$ instead of $\R^2$.
  Let $C$ be the cuboid $C = [a_1, b_1] \times [a_2, b_2] \times [a_3, b_3]$ and $f:C \to \R$ be a function.
  As before, define the Riemann sum
  \begin{equation*}
    \mathcal R(f, P, \Xi)
      = \sum_{i = 0}^{N_1} \sum_{j = 0}^{N_2} \sum_{k = 0}^{N_3} f( \xi_{i, j, k} ) (x_{i+1} - x_i)(y_{i+1} - y_i)(z_{i+1} - z_i)
  \end{equation*}
  define the Riemann integral of $f$ by taking the limit of Riemann sums:
  \begin{equation*}
    \int_U f \, dV = \lim_{\norm{P} \to 0} \mathcal R(f, P, \Xi).
  \end{equation*}
  Here we use $dV$ (or sometimes $dx \, dy \, dz$ to denote that the integral is a volume (or triple) integral.

  When dealing with unbounded functions over unbounded domains,%
  \footnote{%
    As before, we make the ``niceness'' assumption that the boundary of $U$ is a differentiable surface.
  }
  we use the same limiting procedure.
  If
  \begin{equation*}
    \lim_{R \to \infty} \int_{U \cap B(0, R)} \abs{f} \, dV
  \end{equation*}
  exists and is finite then we define
  \begin{equation*}
    \int_U f \, dV = \lim_{R \to \infty} \int_{U \cap B(0, R)} f \, dV
  \end{equation*}

  We can break a volume integral into three iterated integrals, and Fubini's theorem is still true.
  Rather than restate everything, we do a few examples.

  \begin{example}
    Let $U = B(0, R) \subseteq \R^3$.
    Compute $\int_U 1 \, dV$ and derive a formula for the volume.
  \end{example}
  \begin{proof}[Solution]
    Note
    \begin{align*}
      \int_U 1 \, dV
	&= \int_{x = -R}^R \int_{y = -\sqrt{R^2 - x^2}}^{\sqrt{R^2 - x^2}} \int_{z = -\sqrt{R^2 - x^2 - y^2}}^{\sqrt{R^2 - x^2 - y^2}} 1 \, dz \, dy \, dx
	\\
	&= 2 \int_{x = -R}^R \int_{y = -\sqrt{R^2 - x^2}}^{\sqrt{R^2 - x^2}} \sqrt{R^2 - x^2 - y^2} dy \, dx
	\\
	&= 2 \int_{x = -R}^R \int_{\theta = -\frac{\pi}{2}}^{\frac{\pi}{2}} (R^2 - x^2)\cos^2 \theta \, d\theta \, dx
	= \pi \brak[\Big]{ R^2 x - \frac{x^3}{3} }_{-R}^R
	= \frac{4}{3} \pi R^3
	\qedhere
    \end{align*}
  \end{proof}

  \begin{example}
    Compute the volume of a cylinder.
  \end{example}

  \begin{example}
    Compute the volume of the pyramid bounded by the intersection of the planes $x + y + z = 1$ and the three coordinate planes.
  \end{example}

  \section{Coordinate transformations}

  Let $f$ be a function of $(x, y)$ defined on the domain $U$.
  Let
  \begin{equation*}
    \colvec{x\\y} = \varphi( u, v )
  \end{equation*}
  for some coordinate change function $\varphi:U \to V$.
  We claim
  \begin{equation}\label{eqncc1}
    \int_U f(x, y) \, dx \, dy = \int_V f \circ \varphi( u, v) \, \abs{\det D\varphi} \, du \, dv.
  \end{equation}
  Namely, the integral remains unchanged if we replace $U$ with $V$, make the substitution $(x, y) = \varphi( u, v)$, and replace $dx \, dy$ with $\abs{\det D\varphi} \, du \, dv$.
  This is the change of variables theorem.

  \begin{theorem}[Change of Variables]\label{thmChangeOfVar}
    Let $U, V \subseteq \R^2$ be two domains, $\varphi:V \to U$ be a coordinate change map.
    If $f:U \to \R$ is integrable, then
    \begin{equation*}
      \int_U f \, dA = \int_V f \circ \varphi \, \abs{\det D\varphi} \, dA.
    \end{equation*}
  \end{theorem}

  \begin{remark}
    Recall, a coordinate change transformation is a function $\varphi$ which is \emph{bijective} and differentiable for which $D\varphi$ is invertible at all points in the domain.
  \end{remark}

  \begin{remark}
    Theorem~\ref{thmChangeOfVar} is still true under the following relaxed assumptions on $\varphi$:
    Let $U' \subseteq U$ and $V' \subseteq V$ be obtained by removing \emph{finitely many} differentiable curves or points from $U$ and $V$ respectively.
    If $\varphi:V' \to U'$ is differentiable, bijective and $D\varphi$ is invertible on all of $V'$, then Theorem~\ref{thmChangeOfVar} still holds.
    In typical situations $D \varphi$ will be invertible except for a few isolated points, so checking this won't be the bottle neck.
    Bijectivity, however, can fail in subtle ways and needs to be explicitly checked.
  \end{remark}

  \begin{remark}
    The same result is true for triple integrals.
    Further, for the relaxed assumptions on $\varphi$, $U'$ and $V'$ can be obtained by additionally removing finitely many differentiable surfaces from $U$ and $V$ respectively.
  \end{remark}

  The intuition behind Theorem~\ref{thmChangeOfVar} is as follows:
  First if $R$ is any rectangle, and $T:\R^2 \to \R^2$ is a linear transformation, then we know that
  \begin{equation*}
    \area(R) = \abs{\det T}
  \end{equation*}
  Now, divide $V$ into many small non-overlapping regions $R_{i,j}$ and set $R_{i,j}' = \varphi(R_{i,j})$.
  Since $\varphi$ is bijective, the regions $R_{i,j}'$ must also be non-overlapping and cover all of $U$.
  If $R_{i,j}$ are small enough, $\varphi$ can be approximated by an affine function (using $D\varphi$) and hence we expect
  \begin{equation*}
    \area(R'_{i,j}) \approx \area(R_{i,j}) \abs{\det D\varphi_{\xi_{i,j}}}
  \end{equation*}
  where $\xi_{i,j} \in R_{i, j}$.
  Multiplying by $f$, summing and taking limits suggests the formula~\eqref{eqncc1} as claimed.

  \begin{example}[Polar Coordinates]
    Let $(x, y) = \varphi(r, \theta) = (r \cos \theta, r \sin \theta)$.
    Then $\abs{\det D\varphi} = r$ and hence when transforming area integrals to polar coordinates, we replace $dx \, dy$ with $r \, dr \, d\theta $.
  \end{example}

  \begin{example}
    Compute the area of a circle of radius $r$.
  \end{example}

  \begin{example}
    Show $\int_{x^2 + y^2 > 1} \frac{1}{(x^2 + y^2)^{p/2}} \, dA < \infty$ if and only if $p > 2$.
  \end{example}

  \begin{example}
    Show $\int_{-\infty}^\infty e^{-x^2} \, dx = \sqrt{\pi}$.
  \end{example}

  \begin{example}[Spherical coordinates]
    Let
    \begin{equation*}
      (x, y, z) = \varphi( r, \theta, \phi) = (r \sin \phi \cos \theta, r \sin \phi \sin \theta, r \cos \phi ).
    \end{equation*}
    From homework we know $\abs{\det D\varphi} = r^2 \sin \phi$, and hence when transforming volume integrals into spherical coordinates we replace $dV$ with $r^2 \sin \phi \, dr \, d\phi \, d\theta$.
  \end{example}

  \begin{example}
    Compute the volume of a solid sphere.
  \end{example}

  \begin{example}
    Show $\int_{x^2 + y^2 + z^2> 1} \frac{1}{(x^2 + y^2 + z^2)^{p/2}} \, dV < \infty$ if and only if $p > 3$.
  \end{example}

  \begin{example}
    Compute the volume of a cylinder.
  \end{example}

  \begin{example}
    Compute the volume of a cone with an oddly shaped base.
  \end{example}

  \begin{remark}[Failure of bijectivity]
    Here is an example where the failure of bijectivity gives a ``weird'' result.
    Consider the coordinate change
    \begin{equation*}
      \colvec{x\\y} = \varphi(u, v) = \colvec{u^2 - v^2\\2 u v },
    \end{equation*}
    for which $\abs{\det \varphi} = 4(u^2 + v^2) = 4(x^2 + y^2)^{1/2}$.
    By making $x, y$ arbitrarily large (or small) we can do the same for $u$ and $v$.
    So one might hastily write
    \begin{equation*}
      \int_{\R^2} f(x, y) \, dx \, dy = 4 \int_{\R^2} f \circ \varphi(u, v) \, (u^2 + v^2) \, du \, dv.
    \end{equation*}
    This doesn't yield the right answer though!
    Indeed, choosing
    \begin{equation*}
      f(x, y)
	= \frac{\exp\paren{-\sqrt{x^2 + y^2}}}{\sqrt{x^2 + y^2}}
	= \frac{\exp\paren{-(u^2 + v^2)}}{u^2 + v^2}
    \end{equation*}
    yields
    \begin{equation*}
      \int_{\R^2} f(x, y) \, dx \, dy = 2\pi
      \quad\text{and}\quad
      4 \int_{\R^2} f \circ \varphi(u, v) \, (u^2 + v^2) \, du \, dv = 4\pi.
    \end{equation*}

    What failed here is exactly bijectivity.
    You can explicitly solve and check that for every $(x, y) \neq (0, 0)$ there exist exactly two values of $(u, v)$ for which $\varphi(u, v) = (x, y)$.
    Indeed, choosing
    \begin{equation*}
      H = \set{ (u, v) \st u > 0 }
      \quad\text{and}
      V = \set{(x, y) \st y \neq 0 \text{ or } x > 0 },
    \end{equation*}
    we see now that $\varphi:H \to V$ is a coordinate transformation.
    Of course $V$ is simply $\R^2$ with a half line removed, but $H$ is ``half'' of $\R^2$.
    With these domains, we of course have the identity
    \begin{equation*}
      \int_{\R^2} f(x, y) \, dx \, dy = 4 \int_H f \circ \varphi(u, v) \, (u^2 + v^2) \, du \, dv,
    \end{equation*}
    and you can explicitly verify this for the specific choice of $f$ above.
  \end{remark}
\end{document}
