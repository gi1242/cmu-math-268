#! /bin/bash
# Created   : Mon 12 Oct 2015 09:42:34 PM EDT
# Modified  : Mon 13 Jan 2020 09:54:01 AM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

# Precomplied headers don't work with subfiles anymore. Just exit
exit 0


pdflatex -ini -jobname=preamble '&pdflatex preamble.tex\dump'
pdflatex -ini -jobname=subfile '&pdflatex subfile.tex\dump'
