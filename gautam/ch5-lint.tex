% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{4}\fi
  \chapter{Line Integrals}%endchapter

  \section{Line integrals}

  \begin{definition}
    If a force $F$ acting on a body produces an instantaneous displacement $v$, then the work done by the force is $F \cdot v$.
  \end{definition}

  Let $\Gamma \subseteq \R^3$ be a curve, with a given direction of traversal, and $F:\R^3 \to \R^3$ be a (vector) function.
  Here $F$ represents the force that acts on a body and pushes it along the curve $\Gamma$.
  The work done by the force can be approximated by
  \begin{equation*}
    W = \sum_{i = 0}^{N-1} F( x_i ) \cdot ( x_{i+1} - x_i )
  \end{equation*}
  where $x_0$, $x_1$, \dots, $x_{N-1}$ are $N$ points on $\Gamma$, \emph{chosen along the direction of traversal}.
  The limit as the largest distance between neighbours approaches $0$ is defined to be the line integral.

  \begin{definition}
    Let $\Gamma \subseteq \R^d$ be a curve (with a given direction of traversal), and $F:\Gamma \to \R^d$ be a (vector) function.
    The \emph{line integral} of $F$ over $\Gamma$ is defined to be
    \begin{equation*}
      \int_\Gamma F \cdot d\ell = \lim_{\norm{P} \to 0}
	\sum_{i = 0}^{N-1} F( x_i ) \cdot ( x_{i+1} - x_i ).
    \end{equation*}
    Here $P = \set{x_0, x_1, \dots, x_{N-1}}$, the points $x_i$ are chosen along the direction of traversal, and $\norm{P} = \max \abs{x_{i+1} - x_i}$.
  \end{definition}

  \begin{remark}
    If $F = (F_1, \dots, F_d)^T$, where $F_i:\Gamma \to \R$ are functions, then one often writes the line integral in the \emph{differential form} notation as
    \begin{equation*}
      \int_\Gamma F \cdot d\ell
	= \int_\Gamma F_1 \, dx_1 + \cdots + F_d \, dx_d
	= \int_\Gamma \sum_{i = 1}^d F_i \, dx_i.
    \end{equation*}
  \end{remark}

  \begin{proposition}\label{ppnLineIntParam}
    If $\gamma:[a, b] \to \R^d$ is a parametrization of $\Gamma$ (in the direction of traversal), then
    \begin{equation}\label{eqnLineIntDef}
      \int_\Gamma F \cdot d\ell = \int_a^b F \circ \gamma(t) \cdot \gamma'(t) \, dt
    \end{equation}
  \end{proposition}

  In the differential form notation (when $d = 2$) say
  \begin{equation*}
    F = \colvec{f\\g}
    \quad\text{and}\quad
    \gamma(t) = \colvec{x(t)\\y(t)},
  \end{equation*}
  where $f, g: \Gamma \to \R$ are functions.
  Then Proposition~\ref{ppnLineIntParam} says
  \begin{equation*}
    \int_\Gamma F \cdot d\ell
      = \int_\Gamma f \, dx + g \, dy
      = \int_\Gamma \paren[\big]{
	  f(x(t), y(t)) \, x'(t) + 
	  g(x(t), y(t)) \, y'(t) } \, dt
  \end{equation*}

  \begin{remark}
    Sometimes~\eqref{eqnLineIntDef} is used as the definition of the line integral.
    In this case, one needs to verify that this definition is \emph{independent} of the parametrization.
    Since this is a good exercise, we'll do it anyway a little later.
  \end{remark}

  \begin{example}
    Suppose a body of mass $M$ is placed at the origin.
    The force experienced by a body of mass $m$ at the point $x \in \R^3$ is given by $F(x) = \frac{ -GM x}{\abs{x}^3}$, where $G$ is the \emph{gravitational constant}.
    Compute the work done when the body is moved from $a$ to $b$ along a straight line.
  \end{example}
  \begin{proof}[Solution]
    Let $\Gamma$ be the straight line joining $a$ and $b$.
    Clearly $\gamma:[0,1] \to \Gamma$ defined by $\gamma(t) = a + t(b-a)$ is a parametrization of $\Gamma$.
    Now
    \begin{equation*}
      W
	= \int_\Gamma F \cdot \, d\ell
	= -GMm \int_0^1 \frac{\gamma(t)}{\abs{\gamma(t)}^3} \cdot \gamma'(t) \, dt
	= \frac{GMm}{\abs{b}} - \frac{GMm}{\abs{a}}.
	\qedhere
    \end{equation*}
  \end{proof}
  \begin{remark}
    If the line joining through $a$ and $b$ passes through the origin, then some care has to be taken when doing the above computation.
    We will see later that gravity is a \emph{conservative force}, and that the above line integral only depends on the endpoints and not the actual path taken.
  \end{remark}

  \section{Parametrization invariance and arc length}

  So far we have always insisted all curves and parametrizations are differentiable or $C^1$.
  We now relax this requirement and subsequently only assume that  all curves (and parametrizations) are \emph{piecewise differentiable}, or \emph{piecewise $C^1$}.
  \begin{definition}
    A function $f:[a, b] \to \R^d$ is called \emph{piecewise $C^1$} if there exists a finite set $F \subseteq [a, b]$ such that $f$ is $C^1$ on $[a, b] -F$, and further both left and right limits of $f$ and $f'$ exist at all points in $F$.
  \end{definition}

  \begin{definition}
    A (connected) curve $\Gamma$ is \emph{piecewise $C^1$} if it has a parametrization which is continuous \emph{and} piecewise $C^1$.
  \end{definition}

  \begin{remark}
    A piecewise $C^1$ function need not be continuous.
    But curves are always assumed to be at least continuous; so for notational convenience, we define a piecewise $C^1$ curve to be one which has a parametrization which is both continuous and piecewise $C^1$.
  \end{remark}

  \begin{example}
    The boundary of a square is a piecewise $C^1$ curve, but not a differentiable curve.
  \end{example}
  \begin{proposition}[Parametrization invariance]
    If $\gamma_1:[a_1, b_1] \to \Gamma$ and $\gamma_2:[a_2, b_2] \to \Gamma$ are two parametrizations of $\Gamma$ that traverse it in the same direction, then
    \begin{equation*}
      \int_{a_1}^{b_1} F \circ \gamma_1(t) \cdot \gamma_1'(t) \, dt
      =
      \int_{a_2}^{b_2} F \circ \gamma_2(t) \cdot \gamma_2'(t) \, dt.
    \end{equation*}
  \end{proposition}
  \begin{proof}
    Let $\varphi:[a_1, b_1] \to [a_2, b_2]$ be defined by $\varphi = \gamma_2^{-1} \circ \gamma_1$.
    Since $\gamma_1$ and $\gamma_2$ traverse the curve in the same direction, $\varphi$ must be increasing.
    One can also show (using the inverse function theorem) that  $\varphi$ is continuous and piecewise $C^1$.
    Now
    \begin{equation*}
      \int_{a_2}^{b_2} F \circ \gamma_2(t) \cdot \gamma_2'(t) \, dt
	= \int_{a_2}^{b_2} F( \gamma_1( \varphi(t) ) ) \cdot \gamma_1'( \varphi(t) ) \varphi'(t) \, dt.
    \end{equation*}
    Making the substitution $s = \varphi(t)$ finishes the proof.
  \end{proof}

  \begin{definition}
    If $\Gamma \subseteq \R^d$ is a piecewise $C^1$ curve, then
    \begin{equation*}
      \arclen(\Gamma) = \lim_{\norm{P} \to 0} \sum_{i = 0}^N \abs{x_{i+1} - x_i},
    \end{equation*}
    where as before $P = \set{x_0, \dots, x_{N-1}}$.
    More generally, if $f: \Gamma \to \R$ is any scalar function, we define%
    \footnote{Unfortunately $\int_\Gamma f \, \abs{d\ell}$ is also called the line integral. To avoid confusion, we will call this the  \emph{line integral with respect to arc-length} instead.}
    \begin{equation*}
      \int_\Gamma f \, \abs{d \ell}
	\defeq \lim_{\norm{P} \to 0} \sum_{i = 0}^N f(x_i) \, \abs{x_{i+1} - x_i},
    \end{equation*}
  \end{definition}

  The \emph{arc length} of a curve can be computed by taking the line integral of the unit tangent vector.
  \begin{proposition}
    Let $\Gamma \subseteq \R^d$ be a piecewise $C^1$ curve, $\gamma:[a, b] \to \R$ be any parametrization (in the given direction of traversal), $f:\Gamma \to \R$ be a (scalar) function, and $\tau:\Gamma \to \R^d$ is the unit tangent vector (i.e. $\abs{\tau} \equiv 1$ and $\tau$ is always tangent to $\Gamma$) along the direction of traversal.
    Then
    \begin{equation*}
      \int_\Gamma f \, \abs{d\ell}
	= \int_\Gamma f \tau \cdot d\ell
	= \int_a^b f(\gamma(t)) \, \abs{\gamma'(t)} \, dt,
    \end{equation*}
    and consequently
    \begin{equation*}
      \arclen(\Gamma)
	= \int_\Gamma 1 \, \abs{d\ell}
	= \int_a^b \abs{\gamma'(t)} \, dt.
    \end{equation*}
  \end{proposition}

  \begin{example}
    Compute the circumference of a circle of radius $r$. 
  \end{example}

  \begin{remark}
    A very useful way to describe curves is to parametrize them by arc length.
    Namely, let $\gamma(s) \in \Gamma$ be the unique point so that the portion of $\Gamma$ traversed up to the point $\gamma(s)$ has arc length exactly $s$.
  \end{remark}


  \section{The fundamental theorem}
  \begin{theorem}[Fundamental theorem for line integrals]
    Suppose $U \subseteq \R^d$ is a domain, $\varphi:U \to \R$ is $C^1$ and $\Gamma \subseteq \R^d$ is any differentiable curve that starts at $a$, ends at $b$ and is completely contained in $U$.
    Then
    \begin{equation*}
      \int_\Gamma \grad \varphi \cdot d\ell = \varphi(b) - \varphi(a).
    \end{equation*}
  \end{theorem}
  \begin{proof}
    Let $\gamma:[0, 1] \to \Gamma$ be a parametrization of $\Gamma$.
    Note
    \begin{equation*}
      \int_\Gamma \grad \varphi \cdot d\ell
	= \int_0^1 \grad \varphi(\gamma(t)) \cdot \gamma'(t) \, dt
	= \int_0^1 \frac{d}{dt} \varphi(\gamma(t)) \, dt
	= \varphi(b) - \varphi(a).\qedhere
    \end{equation*}
  \end{proof}

  \begin{definition}
    A \emph{closed curve} is a curve that starts and ends at the same point.
    A \emph{simple closed curve} is a closed curve that never crosses itself.
    (More precisely, a simple closed curve is a compact $1$-dimensional manifold with no boundary.)
  \end{definition}

  If $\Gamma$ is a closed curve, then line integrals over $\Gamma$ are denoted by
  \begin{equation*}
    \oint_\Gamma F \cdot d\ell.
  \end{equation*}

  \begin{corollary}\label{clyLintGrad}
    If $\Gamma \subseteq \R^d$ is a closed curve, and $\varphi:\Gamma \to \R$ is $C^1$,  then
    \begin{equation*}
      \oint_\Gamma \grad \varphi \cdot d\ell = 0.
    \end{equation*}
  \end{corollary}

  \begin{definition}
    Let $U \subseteq \R^d$, and $F:U \to \R^d$ be a vector function.
    We say $F$ is a \emph{conservative force} (or \emph{conservative vector field}) if
    \begin{equation*}
      \oint F \cdot d\ell = 0,
    \end{equation*}
    for all closed curves $\Gamma$ which are completely contained inside $U$.
  \end{definition}

  Clearly if $F = - \grad V$ for some $C^1$ function $V:U \to \R$, then $F$ is conservative.
  The converse is also true provided $U$ is \emph{simply connected}, which we'll return to later.

  \begin{example}
    If $\varphi$ fails to be $C^1$ even at one point, the above can fail quite badly.
    Let $\varphi(x, y) = \tan^{-1}(y/x)$, extended to $\R^2 - \set{(x, y) \st x \leq 0 }$ in the usual way.
    Then
    \begin{equation*}
      \grad \varphi = \frac{1}{x^2 + y^2} \colvec{ -y \\ x }
    \end{equation*}
    which is defined on $\R^2 - (0, 0)$.
    In particular, if $\Gamma = \set{ (x, y) \st x^2 + y^2 = 1}$, then $\grad \varphi$ is defined on all of $\Gamma$.
    However, you can easily compute
    \begin{equation*}
      \oint_\Gamma \grad \varphi \cdot d\ell = 2 \pi \neq 0.
    \end{equation*}
    The reason this doesn't contradict the previous corollary is that Corollary~\ref{clyLintGrad} requires $\varphi$ itself to be defined on all of $\Gamma$, and not just $\grad \varphi$!
    This example leads into something called the \emph{winding number} which we will return to later.
  \end{example}

  \section{Greens theorem}
  \begin{theorem}[Greens Theorem]\label{thmGreens}
    Let $\Omega \subseteq \R^2$ be a bounded domain whose exterior boundary is a piecewise $C^1$ curve $\Gamma$.
    If $\Omega$ has holes, let $\Gamma_1$, \dots, $\Gamma_N$ be the interior boundaries.
    If $F:\bar \Omega  \to \R^2$ is $C^1$, then
    \begin{equation*}
      \int_\Omega \paren[\big]{\partial_1 F_2 - \partial_2 F_1} \, dA
	= \oint_{\Gamma} F \cdot d\ell + \sum_{i = 1}^N \oint_{\Gamma_i} F \cdot d\ell,
    \end{equation*}
    where all line integrals above are computed by traversing the exterior boundary \emph{counter clockwise}, and every interior boundary \emph{clockwise}.
  \end{theorem}
  \begin{remark}
    A common convention is to denote the \emph{boundary} of $\Omega$ by $\partial \Omega$ and write
    \begin{equation*}
      \partial \Omega = \Gamma \cup \paren[\big]{ \bigcup_{i = 1}^N \Gamma_i }.
    \end{equation*}
    Then Theorem~\ref{thmGreens} becomes
    \begin{equation*}
      \int_\Omega \paren[\big]{\partial_1 F_2 - \partial_2 F_1} \, dA
	= \oint_{\partial \Omega} F \cdot d\ell,
    \end{equation*}
    where again the exterior boundary is oriented \emph{counter clockwise} and the interior boundaries are all oriented \emph{clockwise}.
  \end{remark}
  \begin{remark}
    In the differential form notation, Greens theorem is stated as
    \begin{equation*}
      \int_\Omega \paren[\Big]{ \partial_x Q - \partial_y P } \, dA
      = \int_{\partial \Omega} P \, dx + Q \, dy,
    \end{equation*}
    $P, Q:\bar \Omega \to \R$ are $C^1$ functions.
    (We use the same assumptions as before on the domain $\Omega$, and orientations of the line integrals on the boundary.)
  \end{remark}
  \begin{remark}
    Note, Greens theorem requires that $\Omega$ is bounded and $F$ (or $P$ and $Q$) is $C^1$ on \emph{all} of $\Omega$.
    If this fails at even one point, Greens theorem need not apply anymore!
  \end{remark}
  \begin{proof}
    The full proof is a little cumbersome.
    But the main idea can be seen by first proving it when $\Omega$ is a square, and then applying a coordinate transformation.
    Indeed, suppose first $\Omega = (0, 1)^2$.
    Then the fundamental theorem of calculus gives
    \begin{equation*}
      \int_\Omega \paren[\big]{ \partial_1 F_2 - \partial_2 F_1 } \, dA
	= \int_{y = 0}^1 \paren[\big]{ F_2(1, y) - F_2(0, y) } \, dy
	  - \int_{x = 0}^1 \paren[\big]{ F_1(x, 1) - F_1(x, 0) } \, dx
    \end{equation*}
    The first integral is the line integral of $F$ on the two vertical sides of the square, and the second one is line integral of $F$ on the two horizontal sides of the square.
    This proves Theorem~\ref{thmGreens} in the case when $\Omega$ is a square.

    Now let $U$ be an arbitrary region for which there exists a $C^2$ coordinate transformation $\varphi:\Omega \to U$, where $\Omega$ is the unit square.
    We assume that $\varphi$ also maps $\partial \Omega$ to $\partial U$ and \emph{preserves the orientation of the boundaries}.
    (One can show that this will imply $\det D\varphi > 0$ in $U$.)
    Now, using Greens theorem on the square,
    \begin{equation*}
      \oint_{\partial U} F \cdot d\ell
	= \oint_{\partial \Omega} (D \varphi)^T F \circ \varphi \cdot d\ell
	= \int_\Omega (\partial_1 G_2 - \partial_2 G_1) \, dA,
    \end{equation*}
    where
    \begin{equation*}
      G = (D \varphi)^T F \circ \varphi = \sum_{i,j} \partial_i \varphi_j F_j \circ \varphi \, e_i
    \end{equation*}

    Now we compute using the chain rule
    \begin{equation*}
	\partial_1 G_2 - \partial_2 G_1
	  = \sum_{i, j} \partial_2 \varphi_j \, \partial_i F_j\bigr|_\varphi \, \partial_1 \varphi_i
	    - \partial_1 \varphi_j \, \partial_i F_j\bigr|_\varphi \, \partial_2 \varphi_i
	  = \paren[\big]{ \partial_1 F_2 - \partial_2 F_1 }\circ \varphi \, \det(D \varphi).
    \end{equation*}
    Thus, by the change of variable theorem,
    \begin{equation*}
      \int_\Omega \paren[\big]{\partial_1 G_2 - \partial_2 G_1} \, dA
	= \int_\Omega \paren[\big]{ \partial_1 F_2 - \partial_2 F_1 }\circ \varphi \, \det(D \varphi) \, dA
	= \int_U \paren[\big]{ \partial_1 F_2 - \partial_2 F_1} \, dA,
    \end{equation*}
    finishing the proof.
  \end{proof}
  \begin{remark}
    The above strategy will only work if the domain has no holes.
    In the presence of holes, you can make one or more cuts and then find a coordinate transformation $\varphi:\Omega \to U$ as above.
    The only difference is now part of the boundary of $\Omega$ will be mapped to the cut you just made.
    The boundary integral over this piece, however, will cancel since it will now be traversed twice in opposite directions.
  \end{remark}
  \begin{corollary}
    If $\Omega \subseteq \R^2$ is bounded with a $C^1$ boundary, then
    \begin{equation*}
      \area(\Omega)
	= \frac{1}{2} \int_{\partial \Omega} \paren[\big]{ -y \, dx + x \, dy }
	= \int_{\partial \Omega} -y \, dx
	= \int_{\partial \Omega} x \, dy
    \end{equation*}
  \end{corollary}
  \begin{remark}
    A \emph{planimeter} is a measuring instrument used to determine the area of an arbitrary two-dimensional shape.
    The operational principle of the planimeter can be proved using the previous corollary.
  \end{remark}
  \begin{corollary}[Surveyor's Formula]
    Let $P \subseteq \R^2$ be a (not necessarily convex) polygon whose vertices, ordered counter clockwise, are $(x_1, y_1)$, \dots, $(x_N, y_N)$.
    Then
    $$
      \area(P) = \frac{(x_1 y_2 - x_2 y_1) + (x_2 y_3 - x_3 y_2) + \cdots + (x_N y_1 - x_1 y_N)}{2}.
    $$
  \end{corollary}
\end{document}
