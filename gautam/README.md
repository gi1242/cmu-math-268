# Compilation Instructions

I use pre-compiled headers to speed up compilation. On a Linux system, just
run the included `pre-compile.sh` before your first LaTeX run. On other
systems you'll have to run the following commands manually:

    pdflatex -ini -jobname=preamble '&pdflatex preamble.tex\dump'
    pdflatex -ini -jobname=subfile '&pdflatex subfile.tex\dump'

You only need to run them once before your first LaTeX run.
Google [LaTeX pre-compiled headers](https://www.google.com/search?q=latex+pre-compiled+headers) for more information.
