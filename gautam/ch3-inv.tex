% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{2}\fi
  \chapter{Inverse and Implicit functions}\label{chrInv}%endchapter
  \section{Inverse Functions and Coordinate Changes}
  Let $U \subseteq \R^d$ be a domain.
  \begin{theorem}[Inverse function theorem]
    If $\varphi: U \to \R^d$ is differentiable at $a$ and $D\varphi_a$ is invertible, then there exists a domains $U', V'$ such that $a \in U' \subseteq U$, $\varphi(a) \in V'$ and $\varphi: U' \to V'$ is bijective.
    Further, the inverse function $\psi:V' \to U'$ is differentiable.
  \end{theorem}

  The proof requires compactness and is beyond the scope of this course.

  \begin{remark}
    The condition $D\varphi_a$ is necessary:
    If $\varphi$ has a differentiable inverse in a neighbourhood of $a$, then $D\varphi_a$ must be invertible. (Proof: Chain rule.)
  \end{remark}

  This is often used to ensure the existence (and differentiability of local coordinates).
  \begin{definition}
    A function $\varphi:U \to V$ is called a (differentiable) coordinate change if $\varphi$ is differentiable and bijective and $D\varphi$ is invertible at every point.
  \end{definition}

  Practically, let $\varphi$ be a coordinate change function, and set $(u, v) = \varphi(x, y)$.
  Let $\psi = \varphi^{-1}$, and we write $(x, y) = \psi( u, v)$.
  Given a function $f:U \to \R$, we treat it as a function of $x$ and $y$.
  Now using $\psi$, we treat $(x, y)$ as functions of $(u, v)$.

  Thus we can treat $f$ as a function of $u$ and $v$, and it is often useful to compute $\partial_u f$ etc. in terms of $\partial_x f$ and $\partial_y f$ and the coordinate change functions.
  By the chain rule:
  \begin{equation*}
    \partial_u f = \partial_x f \partial_u x + \partial_y f \partial_u y,
  \end{equation*}
  and we compute $\partial_u x$, $\partial_u y$ etc. either by directly finding the inverse function and expressing $x, y$ in terms of $u, v$; or implicitly using the chain rule:
  \begin{equation*}
    I = D\psi_\varphi \, D\varphi
      = \begin{pmatrix}
	  \partial_u x & \partial_v x\\
	  \partial_u y & \partial_v y
	\end{pmatrix}
        \begin{pmatrix}
	  \partial_x u & \partial_y u\\
	  \partial_x v & \partial_y v
	\end{pmatrix}
    \implies
      \begin{pmatrix}
	\partial_u x & \partial_v x\\
	\partial_u y & \partial_v y
      \end{pmatrix}
      = \begin{pmatrix}
	    \partial_x u & \partial_y u\\
	    \partial_x v & \partial_y v
	  \end{pmatrix}^{-1}.
  \end{equation*}

  \begin{example}
    Let $u(x, y) = x^2 - y^2$ and $v(x, y) = 2xy$.
    Let $\varphi(x, y) = (u, v)$.
    For any $a \neq 0 \in \R^2$, there exists a small neighbourhood of $a$ in which $\varphi$ has a differentiable inverse.
  \end{example}
  The above tells us that \emph{locally} $x, y$ can be expressed as functions of $u, v$.
  This might not be true globally.
  In the above case we can explicitly solve and find $x, y$:
  \begin{equation}\label{eqnUV}
    x = \paren[\Big]{ \frac{\sqrt{u^2 + v^2} + u}{2} }^{1/2}
    \quad\text{and}\quad
    y = \paren[\Big]{ \frac{\sqrt{u^2 + v^2} - u}{2} }^{1/2}
  \end{equation}
  is one solution. (Negating both, gives another solution.)

  Regardless, even without using the formulae, we can implicitly differentiate and find $\partial_u x$.
  Consequently,
  \begin{equation*}
    \begin{pmatrix}
      \partial_u x & \partial_v x\\
      \partial_u y & \partial_v y
    \end{pmatrix}
    = \begin{pmatrix}
	  \partial_x u & \partial_y u\\
	  \partial_x v & \partial_y v
	\end{pmatrix}^{-1}
    = \begin{pmatrix}
      2x & -2y\\
      2y & 2x
    \end{pmatrix}^{-1}
    = \frac{1}{2(x^2 + y^2)} \begin{pmatrix}
      x & y\\
      -y & x
    \end{pmatrix}.
  \end{equation*}
  It is instructive to differentiate~\eqref{eqnUV} directly and double check that the answers match.

  Polar coordinates is another example, and has been done extensively your homework.

  \section{Implicit functions}

  Let $U \subseteq \R^{d+1}$ be a domain and $f:U \to \R$ be a differentiable function.
  If $x \in \R^d$ and $y \in \R$, we'll concatenate the two vectors and write $(x, y) \in \R^{d+1}$.
  \begin{theorem}[Implicit function theorem]\label{thmImplicit1}
    Suppose $c = f(a, b)$ and $\partial_y f(a, b) \neq 0$.
    Then, there exists a domain $U' \ni a$ and differentiable function $g:U' \to \R$ such that $g(a) = b$ and $f(x, g(x)) = c$ for all $x \in U'$.
    Further, there exists a domain $V' \ni b$ such that $\set{ (x, y) \st x \in U', y \in V', f(x, y) = c } = \set{ (x, g(x)) \st x \in U'}$.
    (In other words, for all $x \in U'$ the equation $f(x, y) = c$ has a unique solution in $V'$ and is given by $y = g(x)$.)
  \end{theorem}

  \begin{remark}
    To see why $\partial_y f \neq 0$ is needed, let $f(x, y) = \alpha x + \beta y$ and consider the equation $f(x, y) = c$.
    To express $y$ as a function of $x$ we need $\beta \neq 0$ which in this case is equivalent to $\partial_y f \neq 0$.
  \end{remark}

  \begin{remark}
    If $d = 1$, one expects $f(x, y) = c$ to some curve in $\R^2$.
    To write this curve in the form $y = g(x)$ using a differentiable function $g$, one needs the curve to never be vertical.
    Since $\grad f$ is perpendicular to the curve, this translates to $\grad f$ never being horizontal, or equivalently $\partial_y f \neq 0$ as assumed in the theorem.
  \end{remark}

  \begin{remark}
    For simplicity we chose $y$ to be the last coordinate above.
    It could have been any other, just as long as the corresponding partial was non-zero.
    Namely if $\partial_i f(a) \neq 0$, then one can locally solve the equation $f(x) = f(a)$ (uniquely) for the variable $x_i$ and express it as a differentiable function of the remaining variables.
  \end{remark}

  \begin{example}
    $f(x, y) = x^2 + y^2$ with $c = 1$.
  \end{example}

  \begin{proof}[Proof of the implicit function theorem]
    Let $\varphi(x, y) = (x, f(x, y))$, and observe $D\varphi_{(a, b)} \neq 0$. By the inverse function theorem $\varphi$ has a unique local inverse $\psi$.
    Note $\psi$ must be of the form $\psi(x, y) = (x, g(x, y))$.
    Also $\varphi\circ \psi = \text{Id}$ implies $(x, y) = \varphi( x, g(x, y) ) = (x, f(x, g(x,y))$.
    Hence $y = g(x, c)$ uniquely solves $f(x, y) = c$ in a small neighborhood of $(a, b)$.
  \end{proof}

  Instead of $y \in \R$ above, we could have been fancier and allowed $y \in \R^n$.
  In this case $f$ needs to be an $\R^n$ valued function, and we need to replace $\partial_y f \neq 0$ with the assumption that the $n \times n$ minor in $Df$ (corresponding to the coordinate positions of $y$) is invertible.
  This is the general version of the implicit function theorem.
  \begin{theorem}[Implicit function theorem, general case]
    Let $U \subseteq \R^{d + n}$ be a domain, $f:\R^{d+n} \to \R^n$ be a differentiable function, $a \in U$ and $M$ be the $n \times n$ matrix obtained by taking the $i_1^\text{th}$, $i_2^\text{th}$, \dots $i_n^\text{th}$ columns from $Df_a$.
    If $M$ is invertible, then one can locally solve the equation $f(x) = f(a)$ (uniquely) for the variables $x_{i_1}$, \dots, $x_{i_n}$ and express them as a differentiable function of the remaining $d$ variables.
  \end{theorem}

  To avoid too many technicalities, we only state a more precise version of the above in the special case where the $n \times n$ matrix obtained by taking the last $n$ columns of $Df$ is invertible.
  Let's use the notation $(x, y) \in \R^{d +n }$ when $x \in \R^d$ and $y \in \R^n$.
  Now the precise statement is almost identical to Theorem~\ref{thmImplicit1}:

  \begin{theorem}[Implicit function theorem, precise statement in a special case]
    Suppose $c = f(a, b)$ and the $n \times n$ matrix obtained by taking the last $n$ columns of $Df_{a,b}$ is invertible.
    Then, there exists a domain $U' \subseteq \R^d$ containing $a$ and differentiable function $g:U' \to \R^n$ such that $g(a) = b$ and $f(x, g(x)) = c$ for all $x \in U'$.
    Further, there exists a domain $V' \subseteq \R^n$ containing $b$ such that $\set{ (x, y) \st x \in U', y \in V', f(x, y) = c } = \set{ (x, g(x)) \st x \in U'}$.
    (In other words, for all $x \in U'$ the equation $f(x, y) = c$ has a unique solution in $V'$ and is given by $y = g(x)$.)
  \end{theorem}

  \begin{example}
    Consider the equations
    \begin{equation*}
      (x-1)^2 + y^2 + z^2 = 5
      \quad\text{and}\quad
      (x+1)^2 + y^2 + z^2 = 5
    \end{equation*}
    for which $x = 0$, $y = 0$, $z = 2$ is one solution.
    For all other solutions close enough to this point, determine which of variables $x$, $y$, $z$ can be expressed as differentiable functions of the others.
  \end{example}
  \begin{proof}[Solution]
    Let $a = (0, 0, 1)$ and
    \begin{equation*}
      F(x, y, z) = \colvec{
	(x-1)^2 + y^2 + z^2\\
	(x+1)^2 + y^2 + z^2}
    \end{equation*}
    Observe
    \begin{equation*}
      DF_a = \colvec{-2 & 0 & 4\\\phantom{-}2 & 0 & 4},
    \end{equation*}
    and the $2 \times 2$ minor using the first and last column is invertible.
    By the implicit function theorem this means that in a small neighbourhood of $a$, $x$ and $z$ can be (uniquely) expressed in terms of $y$.
  \end{proof}

  \begin{remark}
    In the above example, one can of course solve explicitly and obtain
    \begin{equation*}
      x = 0
      \quad\text{and}\quad
      z = \sqrt{4 - y^2},
    \end{equation*}
    but in general we won't get so lucky.
  \end{remark}
  \section{Tangent planes and spaces}

  Let $f:\R^2 \to \R$ be differentiable, and consider the implicitly defined curve $\Gamma = \set{(x, y) \in \R^2 \st f(x, y) = c }$.
  (Note this is some level set of $f$.)
  Pick $(a, b) \in \Gamma$, and suppose $\partial_y f(a, b) \neq 0$.
  By the implicit function theorem, we know that the $y$-coordinate of this curve can locally be expressed as a differentiable function of $x$.
  In this case the tangent line through $(a, b)$ has slope $\frac{dy}{dx}$.
  
  Directly differentiating $f(x, y) = c$ with respect to $x$ (and treating $y$ as a function of $x$) gives
  \begin{equation*}
    \partial_x f + \partial_y f \frac{dy}{dx} = 0
    \iff \frac{dy}{dx} = \frac{-\partial_x f(a, b)}{\partial_y f(a, b)}.
  \end{equation*}

  Further, note that the normal vector at the point $(a, b)$ has direction $(-\frac{dy}{dx}, 1)$.
  Substituting for $\frac{dy}{dx}$ using the above shows that the normal vector is parallel to $\grad f$.

  \begin{remark}
    Geometrically, this means that $\grad f$ is \emph{perpendicular} to level sets of $f$.
    This is the direction along which $f$ is changing ``the most''.
    (Consequently, the directional derivative of $f$ along directions tangent to level sets is $0$.)
  \end{remark}

  The same is true in higher dimensions, which we study next.
  Consider the surface $z = f(x, y)$, and a point $(x_0, y_0, z_0)$ on this surface.
  Projecting it to the $x$-$z$ plane, this becomes the curve $z = f(x, y_0)$ which has slope $\partial_x f$.
  Projecting it onto the $y$-$z$ plane, this becomes the curve with slope $\partial_y f$.
  The \emph{tangent plane} at the point $(x_0, y_0, z_0)$ is defined to be the unique plane passing through $(x_0, y_0, z_0)$ which projects to a line with slope $\partial_x f(x_0, y_0)$ in the $x$-$z$ plane and projects to a line with slope $\partial_y f( x_0, y_0)$ in the $y$-$z$ plane.
  Explicitly, the equation for the tangent plane is
  \begin{equation*}
    z - z_0 = (x - x_0) \partial_x f(x_0, y_0)  + (y - y_0) \partial_y f(x_0, y_0).
  \end{equation*}

  \begin{remark}
    Consider a curve $\Gamma$ in $\R^2$ and $a \in \Gamma$.
    The usual scenario is that $\Gamma$ ``touches'' the tangent line at $a$ and the continues (briefly) on the same side of the tangent line.
    The exception is of course inflection points, where $\Gamma$ passes through its tangent line.
    In a generic curve, inflection points are usually isolated and this doesn't happen too often.

    In $2D$ however, the picture is quite different.
    A surface will ``touch'' and locally stay on the same side of the tangent plane if the Hessian is either positive definite or negative definite.
    If the Hessian has both a strictly positive and a strictly negative eigenvalue, then the curve will necessarily ``pass through'' the tangent plane at the point of contact.
    Further, it is possible to construct surfaces where this happens at \emph{every single point}.
    One such example is the surface $z = x^2 - y^2$.
  \end{remark}

  \begin{definition}
    The \emph{tangent space} to the surface $z = f(x, y)$ at the point $(x_0, y_0, z_0)$ is defined to be the subspace
    \begin{equation*}
      T = \set[\Big]{ (x, y, z) \in \R^3 \st z = x \partial_x f(x_0, y_0) + y \partial_x f(x_0, y_0) = Df_{(x_0, y_0)} \colvec{x\\y} }
    \end{equation*}
    Elements of the tangent space are said to be \emph{tangent vectors} at the point $(x_0, y_0, z_0)$.
  \end{definition}

  \begin{remark}
    The tangent space is \emph{parallel} to the tangent plane, but shifted so that is passes through the origin (and hence is also a vector subspace).
  \end{remark}

  \begin{remark}
    Clearly the vector $(\partial_x f, \partial_y f, -1)$ is \emph{normal} to the tangent space of the surface $z = f(x, y)$.
  \end{remark}

  Now let $g: \R^3 \to \R$ be differentiable, $c \in \R$ and consider the \emph{implicitly defined} surface $\Sigma = \set{ (x, y, z) \st g(x, y, z) = c}$.
  Note again, this is a level set of $g$.
  Suppose $(x_0, y_0, z_0)$ is a point on this surface and $\partial_z g(x_0, y_0, z_0) \neq 0$.
  Then using the implicit function theorem, the $z$-coordinate of this surface can locally be expressed as a differentiable function of $x$ and $y$ (say $z = f(x, y)$).
  In terms of $f$ we know how to compute the tangent plane and space of $\Sigma$.
  Our aim is to write this directly in terms of $g$.

  \begin{proposition}\label{ppnTSurface}
    Let $a = (x_0, y_0, z_0) \in \Sigma$.
    \begin{itemize}
      \item
	The \emph{tangent space} at $a$ (denoted by $T\Sigma_a$) is exactly $\ker(Dg_a)$.

      \item
	The \emph{tangent plane} at $a$ is $\set{x \in \R^3 \st Dg_a (x - a) = 0 }$.
    \end{itemize}
  \end{proposition}

  Recall elements of the tangent space are called \emph{tangent vectors}.
  If $v \in T\Sigma_a$ then $Dg_a (v) = 0$, and hence the directional derivative of $g$ in the direction $v$ must be $0$.
  Note further $\grad g$ is \emph{normal} to the surface $\Sigma$.
  Both these statements were made earlier, but not explored in detail as we didn't have the implicit function theorem at our disposal.

  \begin{proof}[Proof of Proposition~\ref{ppnTSurface}]
    Substituting $z = f(x, y)$ in $g(x, y, z) = c$ and differentiating with respect to $x$ and $y$ gives
    \begin{equation*}
      \partial_x g + \partial_z g \partial_x f = 0
      \quad\text{and}\quad
      \partial_y g + \partial_z g \partial_y f = 0
    \end{equation*}
    Thus the tangent plane to the surface $g(x, y, z) = c$ at the point $(x_0, y_0, z_0)$ is given by
    \begin{equation*}
      z - z_0 = Df_{(x_0, y_0)} \colvec{x - x_0\\y - y_0}
      \iff
      Dg_{(x_0, y_0, z_0)} \colvec{x - x_0\\y - y_0\\z - z_0} = 0
    \end{equation*}
    The tangent space is given by
    \begin{equation*}
      T = \set[\Big]{ \colvec{x\\y\\z} \st \colvec{x\\y\\z} \cdot \grad g_{(x_0, y_0, z_0)} = 0 }.\qedhere
    \end{equation*}
  \end{proof}

  These generalizes in higher dimensions.
  Without being too precise about the definitions, here is the bottom line:
  \begin{proposition}
    Let $g:\R^{n+d} \to \R^n$ be a differentiable function, $c \in \R^n$ and let $M = \set{x \in \R^{n+d} \st g(x) = c}$.
    Suppose the implicit function theorem applies at all points in $M$.
    Then $M$ is a $d$-dimensional ``surface'' (called a $d$-dimensional manifold).
    At any point $a \in M$, the tangent space is exactly $\ker Dg_a$.
    Consequently, $D_v g(a) = 0$ for all tangent vectors $v$, and $\grad g_1$, \dots $\grad g_n$ are $n$ linearly independent vectors that are orthogonal to the tangent space.
  \end{proposition}
  \section{Parametric curves.}

  \begin{definition}
    Let $\Gamma \subseteq \R^d$ be a (differentiable) closed curve.
    We say $\gamma$ is a (differentiable) parametrization of $\Gamma$ if $\gamma:[a, b] \to \Gamma$ is differentiable, $D\gamma \neq 0$, $\gamma:[a, b) \to \Gamma$ is bijective, $\gamma(b) = \gamma(a)$ and $\gamma'(a) = \gamma'(b)$.
    A curve with a parametrization is called a parametric curve.
  \end{definition}

  \begin{example}
    The curve $x^2 + y^2 = 1$ can be parametrized by $\gamma(t) = (\cos t, \sin t)$ for $t \in [0, 2\pi]$
  \end{example}

  \begin{remark}
    A curve can have many parametrizations.
    For example, $\delta(t) = (\cos t, \sin(-t))$ also parametrizes the unit circle, but runs clockwise instead of counter clockwise.
    Choosing a parametrization requires choosing the direction of traversal through the curve.
  \end{remark}
  
  \begin{remark}
  If $\gamma$ is a curve with endpoints, then we require $\set{\gamma(a), \gamma(b)}$ to be the endpoints of the curve (instead of $\gamma(b) = \gamma(a)$).
  \end{remark}

  \begin{remark}
  If $\gamma$ is an open curve, then we only require $\gamma$ to be defined (and bijective) on $(a, b)$.
  \end{remark}

  \begin{remark}
  While curves can not self-intersect, we usually allow parametric curves to self-intersect.
  This is done by replacing the requirement that $\gamma$ is injective with the requirement that if for $x, y \in (a, b)$ we have $\gamma(x) = \gamma(y)$ then $D\gamma_x$ and $D\gamma_y$ are linearly independent.
  Sometimes, one also allows parametric curves loop back on themselves (e.g. $\gamma(t) = (\cos(t), \sin(t))$ for $t \in \R$.
  \end{remark}

  \begin{definition}
    If $\gamma$ represents a differentiable parametric curve, we define $\gamma' = D\gamma$.
  \end{definition}
  \begin{remark}
     For any $t$, $\gamma'(t)$ is a vector in $\R^d$.
     Think of $\gamma(t)$ representing the position of a particle, and $\gamma'$ to represent the velocity.
  \end{remark}

  \begin{proposition}\label{ppnTParametric}
    Let $\Gamma$ be a curve and $\gamma$ be a parametrization, $a = \gamma(t_0) \in \Gamma$.
    %Suppose $\Gamma \defeq \set{ \gamma(t) \st t \in I }$ is the intersection of two surfaces $S = \set{ f = 0 }$ and $S' = \set{ g = 0 }$ for two differentiable functions $f, g$ for which $\grad f$ and $\grad g$ are linearly independent at $a$.
    Then
    \begin{equation*}
      T\Gamma_a = \Span\set{\gamma'(t_0)}.
    \end{equation*}
    Consequently, tangent line through $a$ is $\set{ \gamma(t_0) + t \gamma'(t_0) \st t \in \R }$.
  \end{proposition}
  
  If we think of $\gamma(t)$ as the position of a particle at time $t$, then the above says that the tangent space is spanned by the \emph{velocity} of the particle.
  That is, the velocity of the particle is always tangent to the curve it traces out.
  However, the acceleration of the particle (defined to be $\gamma''$) \emph{need not} be tangent to the curve!
  In fact if the magnitude of the velocity $\abs{\gamma'}$ is constant, then the acceleration will be \emph{perpendicular} to the curve!

  \begin{proof}[Proof of Proposition~\ref{ppnTParametric}]
    We only do the proof in 3D.
    Write $\Gamma = \set{ f = 0 }$ where $f:\R^3 \to \R^2$ is a differentiable function such that $\rank( Df_a ) = 2$.
    In this case $\Gamma = S^{(1)} \cap S^{(2)}$ where $S^{(i)}$ is the surface $\set{f_i = 0}$.
    Since $\Gamma \subseteq S^{(i)}$, $f_i \circ \gamma = 0$ and hence (by the chain rule) $\gamma'(t) \in \ker(Df_i (a))$.
    By dimension counting this forces
    \begin{equation*}
      T\Gamma_a
	= TS^{(1)}_a \cap TS^{(2)}_a
	= \ker(Df_1(a)) \cap \ker( Df_2(a))
	= \Span\set{\gamma'(t)}. \qedhere
    \end{equation*}
  \end{proof}
  \iffalse
  \begin{proof}
    Say first $\gamma:I \to \R^2$, and $\gamma_1'(a) \neq 0$.
    By the inverse function theorem, $\gamma_1$ has a local inverse near $x_1 = a_1$, and near $a$ our parametric curve is of the form $(x, \gamma_2( \gamma_1^{-1}(x)))$.
    In this case we know that tangent vectors are parallel to the vector $(1, \gamma_2'(\gamma_1^{-1}) (\gamma_1^{-1})')$.
    Since $(\gamma_1^{-1})' = 1/\gamma_1'(\gamma_1^{-1})$ we see $(\gamma_1', \gamma_2')$ is parallel to the tangent vector.
  \end{proof}
  \fi

  \section{Curves, surfaces, and manifolds}
  In the previous sections we talked about tangents to curves and surfaces.
  However, we haven't ever precisely defined what a curve or surface is.
  For the curious, the definitions are here.
  The main result (i.e.\ that $\grad g$ is orthogonal to level sets, and that $\ker(Dg)$  is the tangent space) is still true in arbitrary dimensions.
  %(I might skip this section in class if I do not have time.)

  \begin{definition}
    We say $\Gamma \subseteq \R^n$ is a (differentiable) \emph{curve} if for every $a \in \Gamma$ there exists domains $U \subseteq \R^n$, $V \subseteq \R$ and a differentiable function $\varphi:V \to U$ such that $D\varphi \neq 0$ in $V$ and $U \cap \Gamma = \varphi(V)$.
  \end{definition}
  \begin{remark}
    Many authors insist $V = (0,1)$ or $V = \R$.
    This is equivalent to what we have.
  \end{remark}
  \begin{example}
    If $f:\R \to \R^n$ is a differentiable function, then the graph $\Gamma \subseteq \R^{n+1}$ defined by $\Gamma = \set{ (x, f(x)) \st x \in \R}$ is a differentiable curve.
  \end{example}
  \begin{proposition}
    Let $f:\R^{n+1} \to \R^n$ is differentiable, $c \in \R^n$ and $\Gamma = \set{x \in \R^{n+1} \st f(x) = c }$ be the level set of $f$.
    If at every point in $\Gamma$, the matrix $Df$ has rank $n$ then $\Gamma$ is a curve.
  \end{proposition}
  \begin{proof}
    Let $a \in \Gamma$.
    Since $\rank(Df_a) = d$, there must be $d$ linearly independent columns. 
    For simplicity assume these are the first $d$ ones.
    The implicit function theorem applies and guarantees that the equation $f(x) = c$ can be solved for  $x_1, \dots, x_n$, and each $x_i$ can be expressed as a differentiable function of $x_{n+1}$ (close to $a$).
    That is, there exist open sets $U' \subseteq \R^n$, $V' \subseteq \R$ and a differentiable function $g$ such that $a \in U' \times V'$ and $\Gamma \cap (U' \times V')  = \set{(g(x_{n+1}), x_{n+1}) \st x_{n+1} \in V' }$.
  \end{proof}

  Surfaces in higher dimensions are defined similarly.
  \begin{definition}
    We say $\Sigma \subseteq \R^n$ is a (differentiable) \emph{surface} if for every $a \in \Sigma$ there exists domains $U \subseteq \R^n$, $V \subseteq \R^2$ and a differentiable function $\varphi:V \to U$ such that $\rank(D\varphi) = 2$ at every point in $V$ and $U \cap \Sigma = \varphi(V)$.
  \end{definition}

  The difference from a curve is that now $V \subseteq \R^2$ and not $\R$.

  \begin{definition}
    We say $M \subseteq \R^n$ is a $d$-dimensional (differentiable) \emph{manifold} if for every $a \in M$ there exists domains $U \subseteq \R^n$, $V \subseteq \R^d$ and a differentiable function $\varphi:V \to U$ such that $\rank(D\varphi) = d$ at every point in $V$ and $U \cap M = \varphi(V)$.
  \end{definition}
  \begin{remark}
    For $d = 1$ this is just a curve, and for $d = 2$ this is a surface.
  \end{remark}

  \begin{remark}
    If $d = 1$ and $\Gamma$ is a connected, then there exists an interval $U$ and an injective differentiable function $\gamma:U \to \R^n$ such that $D\gamma \neq 0$ on $U$ and $\gamma(U) = \Gamma$.
    If $d > 1$ this is no longer true: even though near every point the surface is a differentiable image of a rectangle, the entire surface need not be one.
  \end{remark}

  As before $d$-dimensional manifolds can be obtained as level sets of functions $f:\R^{n+d} \to \R^d$ provided we have $\rank(Df) = d$ on the entire level set.
  \begin{proposition}
    Let $f:\R^{n+d} \to \R^n$ is differentiable, $c \in \R^n$ and $\Gamma = \set{x \in \R^{n+1} \st f(x) = c }$ be the level set of $f$.
    If at every point in $\Gamma$, the matrix $Df$ has rank $d$ then $\Gamma$ is a $d$-dimensional manifold.
  \end{proposition}

  The results from the previous section about tangent spaces of implicitly defined manifolds generalize naturally in this context.
  \begin{definition}
    Let $U \subseteq \R^d$, $f:U \to R$ be a differentiable function, and $M = \set{(x, f(x)) \in \R^{d+1} \st x \in U }$ be the graph of $f$.
    (Note $M$ is a $d$-dimensional manifold in $\R^{d+1}$.)
    Let $(a, f(a)) \in M$.
    \begin{itemize}
      \item
	The \emph{tangent ``plane''} at the point $(a, f(a))$ is defined by
	\begin{equation*}
	  \set{ (x, y) \in \R^{d+1} \st y = f(a) + Df_a (x - a) }
	\end{equation*}

      \item
	The \emph{tangent space} at the point $(a, f(a))$ (denoted by $TM_{(a,f(a))}$) is the subspace defined by
	\begin{equation*}
	  TM_{(a, f(a))} = \set{ (x, y) \in \R^{d+1} \st y = Df_a x }.
	\end{equation*}
    \end{itemize}
  \end{definition}
  \begin{remark}
    When $d = 2$ the tangent plane is really a plane.
    For $d = 1$ it is a line (the tangent line), and for other values it is a $d$-dimensional hyper-plane.
  \end{remark}

  \begin{proposition}
    Suppose $f:\R^{n+d} \to \R^n$ is differentiable, and the level set $\Gamma = \set{ x \st f(x) = c}$ is a $d$-dimensional manifold.
    Suppose further that $Df_a$ has rank $n$ for all $a \in \Gamma$.
    Then the tangent space at $a$ is precisely the kernel of $Df_a$, and the vectors $\grad f_1$, \dots $\grad f_n$ are $n$ linearly independent vectors that are normal to the tangent space.
  \end{proposition}

  \section{Constrained optimization.}

  Consider an implicitly defined surface $S = \set{g = c}$, for some $g:\R^3 \to \R$.
  Our aim is to maximise or minimise a function $f$ \emph{on this surface}.
  \begin{definition}
    We say a function $f$ attains a local maximum at $a$ on the surface $S$, if there exists $\epsilon > 0$ such that $\abs{x - a} < \epsilon$ and $x \in S$ imply $f(a) \geq f(x)$.
  \end{definition}
  \begin{remark}
    This is sometimes called constrained local maximum, or local maximum subject to the constraint $g = c$.
  \end{remark}
  \begin{proposition}
    Suppose $\grad g \neq 0$ at every point on~$S$.
    If $f$ attains a constrained local maximum (or minimum) at $a$ on the surface $S$, then $\exists \lambda \in \R$ such that $\grad f(a) = \lambda \grad g(a)$.
  \end{proposition}
  \begin{proof}[Intuition]
    If $\grad f(a) \neq 0$, then $S' \defeq \set{f = f(a)}$ is a surface.
    If $f$ attains a constrained maximum at $a$ then $S'$ must be tangent to $S$ at the point $a$.
    This forces $\grad f(a)$ and $\grad g(a)$ to be parallel.
  \end{proof}

  \begin{proposition}[Multiple constraints]
    Let $f, g_1$, \dots, $g_n:\R^d \to \R$ be $: \R^d \to \R$ be differentiable.
    If $f$ attains a local maximum (or minimum) at $a$ subject to the constraints $g_1 = c_1$, $g_2 = c_2$, \dots $g_n = c_n$, and $\grad g_1(a)$, \dots, $\grad g_n(a)$ are linearly independent, then $\exists \lambda_1, \dots \lambda_n \in \R$ such that $\grad f(a) = \sum_1^n \lambda_i \grad g_i(a)$.
  \end{proposition}
  \begin{proof}[Proof in a special case]
    Let's assume $n = 1$ and $d = 3$.
    The proof in this case generalizes without much difficulty to the general case, but the notation is much more cumbersome.
    For notational convenience we will also write $g = g_1$.

    By assumption $\grad g(a) \neq 0$ and hence $\partial_i g (a) \neq 0$ for some $i$.
    For notational simplicity, we assume $i = 3$.
    Now, by the implicit function theorem there exists a function $h$ defined in a small neighborhood of $(a_1, a_2)$ such that for all $x$ close to $a$, we have $g(x) = c$  if and only if $x_3 = h(x_1, x_2)$.
    In particular, this means that close to $a$, all points on the surface $\set{g = c}$ are of the form $(x_1, x_2, h(x_1, x_2))$.
    Thus, $f$ attains a constrained local extremum at $a$ if and only if the function $(x_1, x_2) \mapsto f(x_1, x_2, h(x_1, x_2))$ attains an \emph{unconstrained} local extremum at $(a_1, a_2)$.

    Define
    \begin{equation*}
      \varphi(x_1, x_2) \defeq f( x_1, x_2, h(x_1, x_2))\,.
    \end{equation*}
    By the above argument we know that $\varphi$ attains an \emph{unconstrained} local extremum at $(a_1, a_2)$.
    Now we know that unconstrained local extrema are only attained at critical points.
    Thus to finish the proof, all we need to do is compute the critical points of $\varphi$ and check that it yields $\grad f(a) = \lambda \grad g(a)$ for some $\lambda \in \R$.

    To do this, define
    \begin{equation*}
      \psi(x_1, x_2) = (x_1, x_2, h(x_1, x_2))\,,
    \end{equation*}
    and observe $\varphi = f \circ \psi$.
    By the chain rule, we know $D\varphi = Df_\psi D \psi$.
    Transposing this, and using the fact that $D \varphi_{(a_1, a_2)} = 0$, gives
    \begin{equation}\label{e:GradFinKer}
      (D\varphi_a)^T
	= (D\psi_{(a_1, a_2)})^T \grad f(a)
	= 0
      \quad \implies \quad
	\grad f(a) \in \ker\paren[\big]{ (D \psi_{(a_1, a_2)})^T }\,.
    \end{equation}

    Moreover, by construction of $h$, we know $g \circ \psi(x_1, x_2) = c$, and hence $D( g \circ \psi ) = 0$.
    Transposing this and evaluating at $(a_1, a_2)$ gives
    \begin{equation}\label{e:GradGinKer}
      (D\psi_{(a_1, a_2)})^T \grad g(a)
	= 0
      \quad\implies\quad
      \grad g(a) \in \ker\paren[\big]{ D \psi_{(a_1, a_2)}^T }\,.
    \end{equation}

    Observe now that
    \begin{equation*}
      (D \psi)^T =
	\begin{pmatrix}
	  1 & 0 & \partial_1 h\\
	  0 & 1 & \partial_2 h
	\end{pmatrix}\,,
    \end{equation*}
    and hence $\rank( D\psi )^T = 2$.
    Thus by the rank nullity theorem, $\dim \ker( (D\psi)^T )  = 1$.
    Since $\grad g(a) \neq 0$, by assumption and $\grad g(a) \in \ker( D\psi_{(a_1, a_2)} )$ (from~\eqref{e:GradGinKer}), we must have $\ker( (D\psi_{(a_1, a_2)})^T ) = \Span\set{\grad g(a)}$.
    But since $\grad f(a) \in \ker( (D\psi_{(a_1, a_2)})^T )$ (by~\eqref{e:GradFinKer}), this implies there exists $\lambda \in \R$ such that $\grad f(a) = \lambda \grad g(a)$.
  \end{proof}
  \begin{remark}[Proof when $n \neq 1$]
    The main difference in this case is to note that $\grad g_1$, \dots, $\grad g_n$ form a basis of $\ker( (D\psi)^T )$, and use this to conclude the existence of $\lambda_1$, \dots, $\lambda_n$.
  \end{remark}

  To explicitly find constrained local maxima in $\R^d$ with $n$ constraints we do the following:
  \begin{itemize}
    \item
      Simultaneously solve the system of equations
      \begin{gather*}
	\grad f(x) = \lambda_1 \grad g_1(x) + \cdots \lambda_n \grad g_n(x)\\
	g_1(x) = c_1,\\
	\dots\\
	g_n(x) = c_n.
      \end{gather*}

    \item
      The unknowns are the $d$-coordinates of $x$, and the Lagrange multipliers $\lambda_1$, \dots, $\lambda_n$.
      This is $n + d$ variables.

    \item
      The first equation above is a vector equation where both sides have $d$ coordinates.
      The remaining are scalar equations.
      So the above system is a system of $n + d$ equations with $n + d$ variables.

    \item
      The typical situation will yield a finite number of solutions.

    \item
      There is a test involving the \emph{bordered Hessian} for whether these points are constrained local minima / maxima or neither.
      These are quite complicated, and are usually more trouble than they are worth, so one usually uses some ad-hoc method to decide whether the solution you found is a local maximum or not.
  \end{itemize}

  \begin{example}
    Find necessary conditions for $f(x, y) = y$ to attain a local maxima/minima of subject to the constraint $y = g(x)$.
  \end{example}

  Of course, from one variable calculus, we know that the local maxima / minima must occur at points where $g' = 0$.
  Let's revisit it using the constrained optimization technique above.
  \begin{proof}[Solution]
    Note our constraint is of the form $y - g(x) = 0$.
    So at a local maximum we must have
    \begin{equation*}
      \colvec{0\\1} = \grad f = \lambda \grad( y - g(x)) = \colvec{ -g'(x)\\ 1}
      \quad\text{and}\quad
      y = g(x).
    \end{equation*}
    This forces $\lambda = 1$ and hence $g'(x) = 0$, as expected.
  \end{proof}

  \begin{example}
    Maximise $xy$ subject to the constraint $\frac{x^2}{a^2} + \frac{y^2}{b^2} = 1$.
  \end{example}
  \begin{proof}[Solution]
    At a local maximum,
    \begin{equation*}
      \colvec{y\\x}
	= \grad (xy)
	= \lambda \grad\paren[\Big]{\frac{x^2}{a^2} + \frac{y^2}{b^2}}
	= \lambda
	  \begin{pmatrix}
	    2x / a^2\\
	    2y / b^2.
	  \end{pmatrix}
    \end{equation*}
    which forces $y^2 = x^2 b^2 / a^2$.
    Substituting this in the constraint gives $x = \pm a / \sqrt{2}$ and $y = \pm b / \sqrt{2}$.
    This gives four possibilities for $xy$ to attain a maximum.
    Directly checking shows that the points $(a / \sqrt2, b / \sqrt{2})$ and $(-a / \sqrt{2}, -b / \sqrt{2} )$ both correspond to a local maximum, and the maximum value is $a b / 2$.
  \end{proof}

  \begin{proposition}[Cauchy-Schwartz]
    If $x, y \in \R^n$ then $\abs{x \cdot y} \leq \abs{x} \abs{y}$.
  \end{proposition}
  \begin{proof}
    Maximise $x \cdot y$ subject to the constraint $\abs{x} = a$ and $\abs{y} = b$.
  \end{proof}

  \begin{proposition}[Inequality of the means]
    If $x_i \geq 0$, then
    \begin{equation*}
      \frac{1}{n} \sum_1^n x_i \geq \paren[\Big]{ \prod_1^n x_i }^{1/n}.
    \end{equation*}
  \end{proposition}

  \begin{proposition}[Young's inequality]
    If $p, q > 1$ and $1/p + 1/q = 1$ then
    \begin{equation*}
      \abs{xy} \leq \frac{\abs{x}^p}{p} + \frac{\abs{y}^q}{q}.
    \end{equation*}
  \end{proposition}


\end{document}
