% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{0}\fi
  \chapter{Limits and Continuity}\label{chrLimits}%endchapter
  \section{Open sets in \texorpdfstring{$\R^d$}{Rd}.}

  We begin by defining \emph{open sets}.
  As we will see shortly, in order to study limits and derivatives of functions, we need their domains of definition to be open sets.
  \begin{definition}
    A set $U \subset \R^d$ is open if for every $a \in U$ there exists $r > 0$ such that $B(a, r) \subset U$.
  \end{definition}

  Recall, $B(a, r) \subseteq \R^d$ is defined by 
  \begin{equation*}
    B(a, r) \defeq \set{ x \in \R^d \st \abs{x - a} < r}\,,
  \end{equation*}
  is the \emph{open ball} with center $a$ and radius $r$.
  Throughout these notes we use the convention that a vector $x \in \R^d$ has coordinates $(x_1, x_2, \dots, x_d)$.
  Recall $\abs{x} = \paren{\sum x_i^2}^{1/2}$ is the length of the vector $x$.

  \begin{example}
    The empty set and $\R^d$ are both open.
  \end{example}

  \begin{example}
    The ball $B(a, r)$ is open.
  \end{example}
  \begin{proof}
    Let $x \in B(a, r)$, and choose $\epsilon = r - \abs{x - a}$.
    (Notice $\epsilon > 0$, since $\abs{x - a} < r$ by definition of $B(a, r)$.)
    Now by the triangle inequality
    \begin{equation*}
      \abs{y - a} \leq \abs{y - x} + \abs{x - a} < \epsilon + \abs{x - a} = r\,,
    \end{equation*}
    and hence $y \in B(a, r)$.
    This shows $B(x, \epsilon) \subseteq B(a, r)$ and hence $B(a, r)$ is open.
  \end{proof}

  For now, the above should be all that's needed.
  But here are a few related concepts that will come up later on in the semester.
  \begin{definition}
    The \emph{boundary} of a set $A \subseteq \R^d$, denoted by $\partial A$, is defined by
  \begin{equation*}
    \partial A \defeq \set{ x \in \R^d \st \forall \epsilon > 0,\ B(x, \epsilon) \cap A \neq \emptyset \text{ and } B(x, \epsilon) \cap A^c \neq \emptyset }\,.
  \end{equation*}
  \end{definition}
  \begin{example}
    The boundary of a ball $B(a, r) \subseteq \R^d$ is the $d-1$ dimensional sphere $\set{x \in \R^d \st \abs{x - a} = r }$.
  \end{example}

  One of the reasons the notion of boundary arises is because many fundamental theorems in calculus relate integrals of functions on domains to integrals over the boundary of of this domain.

  Another notion that will be important later is that of connectedness.
  Domains of functions are usually open \emph{connected} sets.
  Connectedness, however, is a bit harder to define precisely at this stage.
  Here is the ``official'' definition:

  \begin{definition}
    An open set $U \subset \R^d$ is \emph{connected} if it can not be expressed as the union of two non-empty, disjoint open sets.
  \end{definition}

  While this definition is the ``official'' one, it is a little harder to grasp (e.g. try proving $\R^d$ is connected)!
  Instead we will use the circular, but intuitive definition to work with instead.
  \begin{definition}
    A set $U \subset \R^d$ is connected if for any $x$ and $y$ in $U$, there exists a \emph{continuous} path that connects $x$ and $y$ that stays entirely within the set $U$.
  \end{definition}

  This definition isn't quite legal because we have not yet defined what a continuous path is.

  \begin{definition}
    A domain (sometimes called an open domain) is an open connected set.
  \end{definition}

  Most functions we study will have an open connected set as their domain of definition.
  Moreover, the boundary of this domain will usually be a (piecewise) smooth curve or surface.

  \section{Limits}

  Let $f \colon \R^m \to \R^n$ be a function.
  Intuitively, we say $\lim_{x\to a} f(x) = \ell$ if by making $x$ close enough to $a$, we can make $f(x)$ arbitrarily close to $\ell$.%
  \footnote{
    Note, often people say $\lim_{x\to a} f(x) = \ell$ if as $x$ gets closer to $a$, $f(x)$ gets closer to $\ell$.
    This is wrong!
    If you draw a graph of $f(x) = x \sin (1 / x)$, for instance, you will see that as $x \to 0$, $f(x) \to 0$.
    But as $x$ gets closer to $0$, $f(x)$ certainly \emph{doesn't} get closer to $0$.%
  }
  We make this precise mathematically as follows.

  \begin{definition}
    We say $\lim_{x \to a} f(x) = l$ if for every $\epsilon > 0$, there exists $\delta > 0$ such that $0 < \abs{x - a} < \delta$ implies $\abs{f(x) - l} < \epsilon$.
  \end{definition}

  \begin{remark}
    If $f$ is only defined on an open set $U$, then we also insist $x, a \in U$ above.
  \end{remark}

  The standard theorems about limits (sums, products, quotients) from one variable calculus still hold in this context.
  \begin{proposition}
    Let $U \subseteq \R^d$ be open, and $f, g \colon U \to \R^n$ be two functions.
    Suppose for some $a \in U$, $\lim_{x \to a} f(x) = \ell$ and $\lim_{x\to a} g(x) = m$.
    \begin{enumerate}
      \item
	For any $\alpha \in \R$, $\lim_{x \to a} (f(x) + \alpha g(x)) = \ell + \alpha m$.
      \item
	$\lim_{x\to a} f(x) \cdot g(x) = \ell \cdot m$.

      \item
	If instead $f \colon U \to \R$, then $\lim_{x \to a} f(x) g(x) = l m$.

      \item 
	If $n = 1$ and $m  \neq 0$, then $\lim_{x\to a} (f(x) / g(x)) = \ell / m$.
    \end{enumerate}
  \end{proposition}
  \begin{proof}
    The proofs of these are almost identical to the one dimensional analogues which we assume the reader is familiar with.
    For brevity we only prove the first assertion.
    Let $\epsilon > 0$.
    Since $\lim_{x\to a} f(x) = \ell$, there exists $\delta_1 > 0$ such that $0 < \abs{x - a} < \delta_1$ implies $\abs{f(x) - \ell} < \epsilon / 2$.
    Also, $\lim_{x\to a} g(x) = \ell$, there exists $\delta_2 > 0$ such that $0 < \abs{x - a} < \delta_1$ implies $\abs{g(x) - \ell} < \epsilon / (2(\abs{\alpha} + 1))$.

    Now choose $\delta = \min\set{\delta_1, \delta_2}$.
    Then if $0 < \abs{x - a} < \delta$ we have
    \begin{equation*}
      \abs{f(x) + \alpha g(x) - (\ell + \alpha m)}
	\leq \abs{f(x) - \ell} + \abs{\alpha} \abs{ g(x) - m}
	\leq \frac{\epsilon}{2} + \frac{\abs{\alpha} \epsilon }{ 2 \abs{\alpha} + 1}
	< \epsilon\,.
	\qedhere
    \end{equation*}
  \end{proof}

  Since limits of functions of one variable have been studied previously, we now attempt to reduce limits of functions of several variables to limits of functions of one variable.
  \begin{proposition}\label{ppnLineLimits}
    If $\lim_{x \to a} f(x) = l$ then for every $v \in \R^d$ with $v \neq 0$, we must have $\lim_{t \to 0} f(a + tv) = l$.
  \end{proposition}
  \begin{proof}
    Pick $\epsilon > 0$.
    We know $\exists \delta > 0$ such that $0 < \abs{x - a} < \delta \implies \abs{f(x) - l} < \epsilon$.
    Choose $\delta_1 = \delta / \abs{v}$.
    Now it immediately follows that $0 < t < \delta_1$ implies $\abs{f(a + tv) - l} < \epsilon$.
  \end{proof}

  The converse (surprisingly) is false!
  \begin{example}\label{e:LineLimitsConv}
    Let $f(x) = 1$ if $0 < x_2 < x_1^2$ and $f(x) = 0$ otherwise.
    Then $\lim_{x \to 0} f(x)$ does not exist, but $\lim_{t \to 0} f( t v) = 0$ for all $v \in \R^2 - \set{0}$.
  \end{example}

  \begin{example}\label{ex:x2y}
    Let $f(x) = x_1^2 x_2 / (x_1^4 + x_2^2)$, and $f(0) = 0$.
    Then $\lim_{x \to 0} f(x)$ does not exist, but $\lim_{t \to 0} f( t v) = 0$ for all $v \in \R^2 - \set{0}$.
  \end{example}

  Proposition~\ref{ppnLineLimits} can be used to show that various limits don't exist.
  \begin{example}
    Show that $\displaystyle \lim_{x \to 0} \frac{x_1 x_2}{\abs{x}^2}$ does not exist.
  \end{example}
  \begin{proof}
    Choosing $v_1 = (1, 1)$ and $v_2 = (1, 0)$ we see
    \begin{equation*}
      \lim_{t \to 0} f(tv_1) = \frac{1}{2}
      \quad\text{and}\quad
      \lim_{t \to 0} f(tv_2) = 0 \neq \frac{1}{2}.
    \end{equation*}
    So by Proposition~\ref{ppnLineLimits}, $\lim_{x \to 0} x_1 x_2 / \abs{x}^2$ can not exist.
  \end{proof}

  \section{Continuity}
  Intuitively, a continuous function is one which sends close by points to close by points.
  To define continuity precisely, one needs to use limits (or the $\epsilon$-$\delta$ definition directly).
  \begin{definition}
    Let $U \subset \R^m$ be a domain, and $f:U \to R^d$ be a function.
    We say $f$ is continuous at $a$ if $\lim_{x \to a} f(x) = f(a)$.
  \end{definition}

  \begin{definition}
    If $f$ is continuous at every point $a \in U$, then we say $f$ is continuous on $U$ (or sometimes simply $f$ is continuous).
  \end{definition}

  Again the standard results on continuity from one variable calculus hold.
  Sums, products, quotients (with a non-zero denominator) and composites of continuous functions will all yield continuous functions.

  The notion of continuity gives us a generalization of Proposition~\ref{ppnLineLimits} that is useful is computing the limits along arbitrary curves instead.
  \begin{proposition}\label{p:curvesLim}
    Let $f:\R^d \to \R$ be a function, and $a \in \R^d$.
    Let $\gamma:[0, 1] \to \R^d$ be a \emph{any continuous function} with $\gamma(0) = a$, and $\gamma( t ) \neq a$ for all $t > 0$.
    If $\lim_{x \to a} f(x) = \ell$, then we must have $\lim_{t \to 0} f( \gamma( t) ) = \ell$.
  \end{proposition}
  \begin{proof}
    The proof of this is very similar to the fact that the composition of continuous functions is again continuous.
    Let $\epsilon > 0$ be arbitrary.
    Since $f(x) \to \ell$ as $x \to a$, there exists $\delta > 0$ such that
    \begin{equation}\label{e:lim1}
      \abs{f(x) - \ell} < \epsilon
      \text{ whenever }
      0 < \abs{x - a} < \delta \,.
    \end{equation}
    Now since $\gamma(t) \to a$ as $t \to 0$, there exists $\delta_1$ such that $\abs{\gamma(t) - a} < \delta$ whenever $\abs{t - 0} < \delta_1$.
    Since $\gamma(t) \neq a$ when $t \neq 0$, we can use~\eqref{e:lim1} to guarantee that
    \begin{equation*}
      \abs{f(\gamma(t)) - \ell} < \epsilon,
      \text{ whenever } \abs{t - 0} < \delta_1\,.
      \qedhere
    \end{equation*}
  \end{proof}

  \begin{corollary}\label{c:curves}
    If there exists two continuous functions $\gamma_1, \gamma_2: [0, 1] \to \R^d$ such that for $i \in {1, 2}$ we have $\gamma_i(0) = a$ and $\gamma_i(t) \neq a$ for all $t > 0$.
    If $\lim_{t \to 0} f(\gamma_1(t)) \neq \lim_{t \to 0} f(\gamma_2(t))$ then $\lim_{x \to a} f(x)$ can not exist.
  \end{corollary}

  One can use Corollary~\ref{c:curves} to quickly check that the limit of the function in Example~\ref{ex:x2y} does not exist.

  Finally, we conclude by showing that the converse of Proposition~\ref{p:curvesLim} is true.
  \begin{proposition}\label{p:CurveConv}
    Let $f \colon \R^d \to \R$ be a function, and $a \in \R^d$.
    Suppose for \emph{every} continuous function $\gamma \colon [0, 1] \to \R^d$ such that $\gamma(0) = a$ and $\gamma(t) \neq a$ for all $t >0 0$, we have $\lim_{t \to 0^+} f(\gamma(t)) = \ell$, then we must have $\lim_{x \to a} f(x) = \ell$.
  \end{proposition}

  This result isn't too useful practically of course.
  To use it to prove existence of limits, you would have to test the limit of $f \circ \gamma(t)$ for every continuous function~$\gamma$ as above.
  If you can do this, you can certainly check the existence of $\lim_{x \to a} f(x)$ by other methods.
  The only reason this proposition is stated here is so that you can contrast it with Proposition~\ref{ppnLineLimits}.

  Indeed, we saw (Example~\ref{e:LineLimitsConv}), that the converse of Proposition~\ref{ppnLineLimits} was false.
  Namely, if the limits along every line exist and are equal, it need not imply the full limit exists.
  However, Proposition~\ref{p:CurveConv} shows that if limits along every continuous curve exist and are equal, then the full limit must indeed exist.

  The proof is a bit technical, but is essentially straightforward.
  \begin{proof}[Proof of Proposition~\ref{p:CurveConv}]
    When $d = 1$ the proposition is easily proved by choosing the two curves $a + t$ and $a - t$.
    Thus, we will now assume $d > 1$.
    Suppose for contradiction $\lim_{x \to a} f(x) \neq \ell$.
    Then, there exists $\epsilon > 0$ such that for every $\delta > 0$ there exists $x \in \R^d$ such that $0 < \abs{x - a} < \delta$, but $\abs{f(x) - \ell} \geq \epsilon$.
    For every $n \in \N$, choose $\delta = 1/n$, to obtain a point $x_n \in \R^d$ such that $0 < \abs{x_n - a} < 1/n$, but $\abs{f(x_n) - \ell} \geq \epsilon$.

    Now, we $\gamma$ such that on the interval $[\frac{1}{n+1}, \frac{1}{n}]$, $\gamma$ is a continuous curve connecting $x_{n+1}$ and $x_n$ without passing through~$a$, and staying at most a distance of $1/n$ away from~$a$.
    (This is always possible when $d \geq 2$, and is easy to write down explicitly using two line segments for instance.)
    Define $\gamma(0) = a$.

    It is easy to see that $\gamma$ is a continuous function such that $\gamma(t) \neq a$ for all $t > 0$ and $\lim_{t \to 0} \gamma(t) = a$.
    However, since $\abs{ f(\gamma(1/n)) - \ell } = \abs{f(x_n) - \ell} \geq \epsilon$, and so $\lim_{t \to 0} f(\gamma(t)) \neq \ell$.
    This contradicts the hypothesis of the proposition, concluding the proof.
  \end{proof}
\end{document}
