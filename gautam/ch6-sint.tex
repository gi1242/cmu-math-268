% %&subfile
\input{subfile}
\begin{document}
  \ifstandalonechapter\setcounter{chapter}{5}\fi
  \chapter{Surface Integrals}\label{chrSurf}%endchapter
  \section{Surface integrals}
  Suppose a curved metal plate (or soap film) lies along the surface $\Sigma \subseteq \R^3$, and $f:\Sigma \to \R$ is the density of the plate.
  If we divide the surface $\Sigma$ into many small regions $R_i$, then the mass of the plate can be approximated by
  \begin{equation*}
    M = \sum_i f(\xi_i) \area(R_i),
  \end{equation*}
  where $\xi_i \in R_i$ is some point.

  \begin{definition}
    Let $\Sigma \subseteq \R^3$ be a surface, and $f:\Sigma \to \R$ be a function.
    Define
    \begin{equation*}
      \int_\Sigma f \, dS = \lim_{\norm{P} \to 0} \sum_{i = 1}^N f(\xi_i) \area(R_i),
    \end{equation*}
    where $P$ is a partition of $\Sigma$ into the regions $R_1$, \dots, $R_N$, and $\norm{P}$ is the diameter of the largest region $R_i$.
  \end{definition}

  \begin{remark}
    Other common notation for the surface integral is
    \begin{equation*}
      \int_\Sigma f \, dS
	= \iint_\Sigma f \, dS 
	= \int_\Sigma f \, d\sigma
	= \int_\Sigma f \, dA
    \end{equation*}
  \end{remark}

  As with line integrals, we obtained a formula in terms of a parametrization.
  We follow the same approach for surfaces, but there are a few subtle points that need to be addressed.

  \begin{definition}
    Let $\Sigma \subseteq \R^3$ be a surface.
    We say $\varphi$ is a ($C^1$) parametrization of $\Sigma$ if there exists a domain $U \subseteq \R^2$ such that $\varphi:U \to \Sigma$ is $C^1$, bijective, and $\rank(D\varphi) = 2$ at all points in $U$.
  \end{definition}

  \begin{example}
    Let $U \subseteq \R^2$, $f:U \to \R$ is a function, and $\Sigma$ be the graph of $f$.
    Then $\varphi(x,y) = (x, y, f(x, y))$ is a parametrization of $\Sigma$.
  \end{example}

  \begin{example}
    Define
    \begin{equation*}
      \varphi(\theta, \phi) = \colvec{
	\sin \phi \cos \theta\\
	\sin \phi \sin \theta\\
	\cos \phi
      }
    \end{equation*}
    for $\theta \in (-\pi, \pi)$ and $\phi \in (0, \pi)$.
    Then $\varphi$ parametrizes the surface
    \begin{equation*}
      \Sigma \defeq \set{ x \in \R^3 \st \abs{x}^2 = 1 \; \& \; x_3 \neq \pm 1 },
    \end{equation*}
    which is the unit sphere with an arc joining the north and south poles removed.
  \end{example}

  \begin{remark}
    Typically $U = (0, 1)^2$ is the unit square.
  \end{remark}

  \begin{remark}
    While every curve has a parametrization, not every surface has a parametrization!
    The torus, for instance, can not be parametrized.
    Further, surfaces like the Klein-Bottle, can not even be visualised in three dimensions (but can in four dimensions).
  \end{remark}

  \begin{proposition}\label{ppnSintScalar}
    If $\varphi:U \to \Sigma$ is a parametrization of the surface $\Sigma$, then
    \begin{equation*}
      \int_\Sigma f \, dS
	= \int_U f \circ \varphi \, \abs{\partial_1 \varphi \times \partial_2 \varphi} \, dA.
    \end{equation*}
    Here $\partial_i \varphi = D\varphi \, e_i = (\partial_i \varphi_1, \partial_i \varphi_2, \partial_i \varphi_3)^T$.
  \end{proposition}

  \begin{remark}
    If the surface can not be parametrized, the integral can be computed by breaking up $\Sigma$ into finitely many pieces which \emph{can} be parametrized.
    The formula above will yield an answer that is independent of the chosen parametrization and how you break up the surface (if necessary).
  \end{remark}

  While a rigorous proof is beyond the scope of this course, we provide some intuition here.
  First, we know that if $R \subseteq \R^3$ is a parallelogram whose sides are the vectors $u, v \in \R^3$, then
  \begin{equation*}
    \area(R)
      = \abs{u} \abs{v} \sin \theta
      = \abs{u \times v}
      = \abs*{
      \colvec{
	u_2 v_3 - u_3 v_2\\
	u_3 v_1 - u_1 v_3\\
	u_1 v_2 - u_2 v_3
      }
    }
  \end{equation*}

  Now let $R_{i,j} \subseteq U$ be a small rectangle, and $R_{i,j}' = \varphi(R_{i,j})$.
  Let $R$ be one of these rectangles, and $a$ be the bottom left corner, and $a + h$ be the top right corner.
  Now $R' = \varphi(R)$ is approximately the parallelogram with sides $\partial_1 \varphi(a) h_1$ and $\partial_2 \varphi(a) h_2$, and so
  \begin{equation*}
    \area(R')
      \approx \abs{\partial_1 \varphi \times \partial_2 \varphi} h_1 h_2.
      = \abs{\partial_1 \varphi \times \partial_2 \varphi} \area(R).
  \end{equation*}
  Thus
  \begin{multline*}
    \int_\Sigma f \, dS
      \xleftarrow{\norm{P} \to 0} \sum_{i,j} f(\xi_{i,j}) \area(R_{i,j}')
    \\
      \approx \sum_{i,j} f(\varphi(\eta_{i,j})) \abs{\partial_1 \varphi \times \partial_2 \varphi} \area(R_{i,j})
      \xrightarrow{\norm{P} \to 0} \int_U f \circ \varphi \, \abs{\partial_1 \varphi \times \partial_2 \varphi} \, dA.
  \end{multline*}

  \begin{example}
    Compute the surface area of a sphere of radius $R$.
  \end{example}



  %endsection
  \section{Surface integrals of vector functions}

  \begin{definition}
    We say $(\Sigma, \hat n)$ is an \emph{oriented surface} if $\Sigma \subseteq \R^3$ is a $C^1$ surface, $\hat n: \Sigma \to \R^3$ is a continuous function such that for every $x \in \Sigma$, the vector $\hat n(x)$ is normal to the surface $\Sigma$ at the point $x$, and $\abs{\hat n(x)} = 1$.
  \end{definition}

  \begin{example}
    Let $\Sigma = \set{ x \in \R^3 \st \abs{x} = 1}$, and choose $\hat n(x) = x / \abs{x}$.
  \end{example}

  \begin{remark}
    At any point $x \in \Sigma$ there are exactly two possible choices of $\hat n(x)$.
    An oriented surface simply provides a consistent choice of one of these \emph{in a continuous way} on the \emph{entire surface}.
    Surprisingly this isn't always possible!
    If $\Sigma$ is the surface of a M\"obius strip, for instance, can not be oriented.
  \end{remark}

  \begin{example}
    If $\Sigma$ is the graph of a function, we orient $\Sigma$ by chosing $\hat n$ to always be the unit normal vector with a positive $z$ coordinate.
  \end{example}

  \begin{example}
    If $\Sigma$ is a closed surface, then we will typically orient $\Sigma$ by letting $\hat n$ to be the \emph{outward pointing} normal vector.
  \end{example}

  \begin{definition}
    Let $U \subseteq \R^3$ be a domain.
    We say $u:U \to \R^3$ is a \emph{vector field}.
  \end{definition}

  Typical examples of vector fields are electric and magnetic fields, or the velocity field of a fluid.
  Let $u$ be a vector field representing the velocity of a fluid (i.e. for $x \in  \R^3$, $u(x) \in \R^3$ is the velocity of the fluid at the point $x$.).
  If $\Sigma$ is some oriented surface with unit normal $\hat n$, then the amount of fluid flowing through $\Sigma$ per unit time is exactly
  \begin{equation*}
    \int_\Sigma u \cdot \hat n \, dS.
  \end{equation*}
  Note, both $u$ and $\hat n$ above are \emph{vector functions}, and $u \cdot \hat n: \Sigma \to \R$ is a scalar function.
  The surface integral of this was defined in the previous section.

  \begin{definition}
    Let $(\Sigma, \hat n)$ be an oriented surface, and $u:\Sigma \to \R^3$ be a vector field.
    The \emph{surface integral} of $u$ over $\Sigma$ is defined to be
    \begin{equation*}
      \int_\Sigma u \cdot \hat n \, dS.
    \end{equation*}
  \end{definition}

  \begin{remark}
    Other common notation for the surface integral is
    \begin{equation*}
      \int_\Sigma u \cdot \hat n \, dS
      = \iint_\Sigma u \cdot dS
      = \int_\Sigma u \cdot dS
      = \int_\Sigma u \cdot d\sigma
    \end{equation*}
  \end{remark}

  \begin{proposition}
    Let $\varphi:U \to \Sigma$ be a parametrization of the oriented surface $(\Sigma, \hat n)$.
    Then either
    \begin{equation}\label{eqnSNorm}
      \hat n \circ \varphi
	= \frac{\partial_1 \varphi \times \partial_2 \varphi}{\abs{\partial_1 \varphi \times \partial_2 \varphi}}
    \end{equation}
    on all of $\Sigma$, or
    \begin{equation}\label{eqnSNorm2}
      \hat n \circ \varphi
	= -\frac{\partial_1 \varphi \times \partial_2 \varphi}{\abs{\partial_1 \varphi \times \partial_2 \varphi}}
    \end{equation}
    on all of $\Sigma$.
    Consequently, in the case~\eqref{eqnSNorm} holds, we have
    \begin{equation}\label{eqnSintDef}
      \int_\Sigma u \cdot \hat n \, dS
	= \int_U
	  (u \circ \varphi)  \cdot (\partial_1 \varphi \times \partial_2 \varphi) \, dA.
    \end{equation}
  \end{proposition}
  \begin{proof}
    Clearly~\eqref{eqnSintDef} follows from~\eqref{eqnSNorm} and Proposition~\ref{ppnSintScalar}.
    To prove~\eqref{eqnSNorm}, observe first that the curve $\gamma(t) = \varphi( a + t e_i )$ is contained in the surface $\Sigma$.
    Consequently $\gamma' = \partial_i \varphi$ must be tangent to $\Sigma$ for $i \in  \set{1, 2}$.
    This forces $\partial_1 \varphi \times \partial_2 \varphi$  to be \emph{normal} to $\Sigma$ and hence parallel to $\hat n$.
    Thus
    \begin{equation*}
      s \defeq \hat n \cdot \frac{\partial_1 \varphi \times \partial_2 \varphi}{\abs{\partial_1 \varphi \times \partial_2 \varphi}}
    \end{equation*}
    must be a function that only takes on the values $\pm 1$.
    Since $s$ is also continuous, it must either be identically $1$ or identically $-1$, finishing the proof.
  \end{proof}

  \begin{example}
    Gauss's law sates that the total charge enclosed by a surface $\Sigma$ is given by
    \begin{equation*}
      Q = \epsilon_0 \int_\Sigma  E \cdot dS,
    \end{equation*}
    where $\epsilon_0$ the permittivity of free space, and $E$ is the electric field.
    By convention, the normal vector is chosen to be pointing outward.

    If $E(x) = e_3$, compute the charge enclosed by the top half of the hemisphere bounded by $\abs{x}  = 1$ and $x_3  = 0$.
  \end{example}

  


  \section{Stokes theorem}

  \begin{definition}
    If $F:\R^3 \to \R^3$ is a vector field, we define the \emph{curl of $F$}
    \begin{equation*}
      \curl F \defeq \colvec{
	\partial_2 F_3 - \partial_3 F_2\\
	\partial_3 F_1 - \partial_1 F_3\\
	\partial_1 F_2 - \partial_2 F_1}.
    \end{equation*}
    This is sometimes also denoted by $\operatorname{curl}(F)$.
  \end{definition}

  \begin{remark}
    A mnemonic to remember this formula is to write
    \begin{equation*}
      \curl F = \colvec{\partial_1\\\partial_2\\\partial_3} \times \colvec{F_1\\F_2\\F_3},
    \end{equation*}
    and compute the cross product treating both terms as 3-dimensional vectors.
  \end{remark}

  \begin{remark}\label{rmkBallRotating}
    Let $u:\R^3 \to \R^3$ be a vector field representing the velocity of a fluid.
    The quantity $\curl u$ measures the infinitesimal circulation of the fluid.
    Namely, if a small ball is placed in the fluid, then due to friction between the fluid and the ball's surface, the ball will start rotating.
    Indeed, a counter clockwise rotation about the $x_3$-axis will be produced if $u_2$ is smaller on the left of the ball than the right, or if $u_1$ is larger in the front of the ball than at the back.
    This velocity differential is captured by $\partial_1 u_2 - \partial_2 u_1$, which is exactly the third component of $\curl u$.
    A more precise calculation can be used to show that the rotation axis of the ball (according to the right hand rule), will be parallel to $\curl u$, and the angular speed will be exactly $\abs{\curl u}/2$.
  \end{remark}

  \begin{example}
    If $F(x) = x / \abs{x}^3$, then $\curl F = 0$.
  \end{example}

  \begin{remark}
    In the example above, $F$ is proportional to a gravitational force exerted by a body at the origin.
    We know from experience that when a ball is pulled towards the earth by gravity alone, it doesn't start to rotate; which is consistent with our computation $\curl F  = 0$.
  \end{remark}

  \begin{example}
    If $v(x, y, z) = (\sin z, 0, 0)$, then $\curl v = (0, \cos z, 0 )$.
  \end{example}
  \begin{remark}
    Think of $v$ above as the velocity field of a fluid between two plates placed at $z = 0$ and $z = \pi$.
    A small ball placed closer to the bottom plate experiences a higher velocity near the top than it does at the bottom, and so should start rotating counter clockwise along the $y$-axis.
    This is consistent with our calculation of $\curl v$.
  \end{remark}

  \begin{remark}
    Formally if $F$ is a vector field, then so is $\curl F$.
    However, structurally $\curl F$ is a \emph{2-form}, and not a vector field!
    It is usually identified with a vector field using Hodge duality.
    Since the discussion of differential forms is beyond the scope of this course, we will gloss over this point and simply treat the curl of a vector field as a vector field.
  \end{remark}



  \begin{theorem}[Stokes Theorem]
    Let $U \subseteq \R^3$ be a domain, $(\Sigma, \hat n) \subseteq U$ be a bounded, oriented, piecewise $C^1$, surface whose boundary is the (piecewise $C^1$) curve $\Gamma$.
    If $F:U \to \R^3$ be a $C^1$ vector field, then
    \begin{equation*}
      \int_{\Sigma} \curl F \cdot \hat n \, dS
	= \oint_\Gamma F \cdot d\ell.
    \end{equation*}
    Here $\Gamma$ is traversed in the counter clockwise direction when viewed by an observer standing with his feet on the surface and head in the direction of the normal vector.
  \end{theorem}
  \begin{remark}
    The rule determining the direction of traversal of $\Gamma$ is often called the \emph{right hand rule}.
    Namely, if you put your right hand on the surface with thumb aligned with $\hat n$, then $\Gamma$ is traversed in the pointed to by your index finger.
  \end{remark}

  \begin{remark}
    If the surface $\Sigma$ has holes in it, then (as we did with Greens theorem) we orient each of the holes clockwise, and the exterior boundary counter clockwise following the right hand rule.
    Now Stokes theorem becomes
    \begin{equation*}
      \int_{\Sigma} \curl F \cdot \hat n \, dS
	= \int_{\partial \Sigma} F \cdot d\ell,
    \end{equation*}
    where the line integral over $\partial \Sigma$ is defined to be the sum of the line integrals over each component of the boundary.
  \end{remark}

  \begin{remark}
    If $\Sigma$ is contained in the $x, y$ plane and is oriented by choosing $\hat n = e_3$, then Stokes theorem reduces to Greens theorem.
  \end{remark}

  Stokes theorem allows us to quickly see how the curl of a vector field measures the infinitesimal circulation.
  \begin{proposition}
    Suppose a small, rigid paddle wheel of radius $a$ is placed in a fluid with center at $x_0$ and rotation axis parallel to $\hat n$.
    Let $v:\R^3 \to \R^3$ be the vector field describing the velocity of the ambient fluid.
    If $\omega$ the angular speed of rotation of the paddle wheel about the axis $\hat n$, then
    \begin{equation*}
      \lim_{a \to 0} \omega = \frac{\curl v(x_0) \cdot \hat n}{2}.
    \end{equation*}
  \end{proposition}
  \begin{proof}
    Let $\Sigma$ be the surface of a disk with center $x_0$, radius $a$, and face perpendicular to $\hat n$, and $\Gamma = \partial \Sigma$.
    (Here $\Sigma$ represents the face of the paddle wheel, and $\Gamma$ the boundary.)
    In equilibrium, there is no transfer of momentum from the fluid to the paddle.
    This means that the angular speed $\omega$ will be such that
    \begin{equation*}
      \oint_\Gamma (v - a \omega \hat \tau) \cdot d\ell = 0,
    \end{equation*}
    where $\hat \tau$ is a unit vector tangent to $\Gamma$, pointing in the direction of traversal.
    Consequently
    \begin{equation*}
      \omega
	= \frac{1}{2 \pi a^2} \oint_\Gamma v \cdot d\ell
	= \frac{1}{2 \pi a^2} \int_\Sigma \curl v \cdot \hat n \, dS
	\xrightarrow{a \to 0} \frac{\curl v(x_0) \cdot \hat n}{2}.
	\qedhere
    \end{equation*}
  \end{proof}

  \begin{remark}
    If the axis of the paddle wheel is chosen to maximise the angular velocity, we see that $\hat n$ must be parallel to $\curl v$, and the maximum angular velocity is exactly $\abs{\curl v} / 2$.
  \end{remark}
  \begin{remark}
    Treating a small sphere as a combination of paddle wheels will prove the rotation formula claimed in Remark~\ref{rmkBallRotating}.
  \end{remark}


  \begin{proof}[Proof of Stokes theorem]
    In the case that $\Sigma$ admits a $C^2$ parametrization, we can quickly deduce Stokes theorem from Greens theorem as follows.
    Let $\varphi:U \to \Sigma$ be a $C^1$ parametrization of $\Sigma$ such that $\hat n \cdot (\partial_1 \varphi \times \partial_2 \varphi) > 0$.
    Now
    \begin{equation*}
      \int_\Sigma \curl F \cdot \hat n \, dS
	= \int_U (\curl F) \circ \varphi \cdot (\partial_1 \varphi \times \partial_2 \varphi) \, dA.
    \end{equation*}
    If we define $G: U \to \R^2$ by
    \begin{equation*}
      G = (D\varphi)^T \, (F\circ \varphi),
    \end{equation*}
    then a direct calculation using the chain and product rules shows
    \begin{equation*}
      \partial_1 G_2 - \partial_2 G_1
	= (\curl F) \circ \varphi \cdot (\partial_1 \varphi \times \partial_2 \varphi).
    \end{equation*}
    Consequently
    \begin{equation*}
      \int_\Sigma \curl F \cdot \hat n \, dS
	= \int_U (\partial_1 G_2 - \partial_2 G_1) \, dA
	= \oint_{\partial U} G \, d\ell.
    \end{equation*}
    Parametrising the curve $\partial U$ the same calculation we did in the proof of Greens theorem shows
    \begin{equation*}
      \oint_{\partial U} G \, d\ell = \oint_{\Gamma} F \cdot d\ell,
    \end{equation*}
    finishing the proof.
  \end{proof}

  \section{Conservative and Potential Forces.}

  \begin{definition}
    Let $U \subseteq \R^3$, and $F:U \to \R^3$ be a $C^1$ vector field.
    \begin{itemize}
      \item 
	We say $F$ is a \emph{conservative force} if
	\begin{equation*}
	  \oint_\Gamma F \cdot d\ell = 0,
	\end{equation*}
	for all closed curves $\Gamma$ which are completely contained inside $U$.

      \item
	We say $F$ is a \emph{potential force} there exists a $C^2$ function $V: U \to \R$ such that $F = - \grad V$.
	(The function $V$ is called the potential.)
    \end{itemize}
  \end{definition}

  \begin{definition}
    A domain $U \subseteq \R^3$ is called \emph{simply connected} if for every simple closed curve $\Gamma \subseteq U$, there exists a surface $\Sigma \subseteq U$ whose boundary is exactly the curve $\Gamma$.
  \end{definition}

  We've seen before that any potential force must be conservative.
  We address the converse here.

  \begin{theorem}\label{thmPotentialForce}
    Let $U \subseteq \R^3$ be a simply connected domain, and $F:U \to \R^3$ be a $C^1$ vector field.
    Then $F$ is a conservative force, if and only if $F$ is a potential force, if and only if $\curl F = 0$.
  \end{theorem}

  The physics of conservative and potential forces aside, this result has interesting mathematical content:
  One can easily check that
  \begin{equation*}
    \curl (\grad V) = 0,
  \end{equation*}
  for any $C^2$ function $V$.
  Is the converse true?
  Namely if $\curl F = 0$, must $F = \grad V$ for some function $V:U \to \R$?
  Theorem~\ref{thmPotentialForce} says yes, provided the domain of $F$ is simply connected.

  \begin{proof}[Proof of Theorem~\ref{thmPotentialForce}]
    Clearly, if $F$ is a potential force, equality of mixed partials shows $\curl F = 0$.
    Suppose now $\curl F = 0$.
    By Stokes theorem
    \begin{equation*}
      \oint_\Gamma F \cdot d\ell = \int_{\Sigma} \curl F \cdot \hat n \, dS = 0,
    \end{equation*}
    and so $F$ is conservative.
    Thus to finish the proof of the theorem, we only need to show that a conservative force is a potential force.
    We do this next.

    Suppose $F$ is a conservative force.
    Fix $x_0 \in U$ and define
    \begin{equation*}
      V(x) = -\int_\Gamma F \cdot d\ell,
    \end{equation*}
    where $\Gamma$ is \emph{any} path joining $x_0$ and $x$ that is completely contained in $U$.
    Since $F$ is conservative, we seen before that the line integral above \emph{will not} depend on the path itself but only on the endpoints.

    Now let $h > 0$, and let $\Gamma$ be a path that joins $x_0$ to $a$, and is a straight line between $a$ and $a + h e_1$.
    Then
    \begin{equation*}
      -\partial_1 V(a) = \lim_{h \to 0} \frac{1}{h} \int_{a_1}^{a_1 + h} F_1( a + t e_1) \, dt = F_1(a).
    \end{equation*}
    The other partials can be computed similarly to obtain $F = -\grad V$ concluding the proof.
  \end{proof}

  \begin{remark}
    Let $U = \R^3 - \set{ te_3 \st t \in \R }$, and define $F: U \to \R^3$ by
    \begin{equation*}
      F(x) = \frac{1}{x_1^2 + x_2^2} \colvec{ -x_2\\x_1\\0}.
    \end{equation*}
    It's easy to check that $F \in C^1(U)$ and $\curl F = 0$.
    However, we claim there does not exist any $V:U \to \R$ such that $F = -\grad V$.
    To see this let $\Gamma$ be the unit circle with center $0$ contained in the $x_1$-$x_2$ plane.
    A calculation we've done before shows
    \begin{equation*}
      \oint_\Gamma F \cdot d\ell = 2\pi \neq 0.
    \end{equation*}
    But by the fundamental theorem we know $\oint \grad V \cdot d\ell = 0$ for any closed curve, and thus $F$ can not equal $-\grad V$ for any $V \in C^1(U)$.
  \end{remark}

  \section{Divergence Theorem}

  \begin{definition}
    If $v:\R^3 \to \R^3$ is a $C^1$, vector field we define the \emph{divergence} of $v$ by
    \begin{equation*}
      \dv v = \sum_{i = 1}^3 \partial_i v_i.
    \end{equation*}
  \end{definition}
  \begin{remark}
    The divergence is often denoted by $\operatorname{div}(v)$, and measures the infinitesimal \emph{outward flux} of a vector field at a given point.
    Indeed, suppose $v$ represents the velocity field of a fluid and consider a small imaginary cube placed in the fluid.
    The difference in the horizontal components of the velocity on the right and left will contribute towards the horizontal outward flux, and is captured by the $\partial_1 v_1$ term.
    Similarly the $\partial_2 v_2$ and $\partial_3 v_3$ terms capture the outward fluxes parallel to the $x_2$ and $x_3$ axes respectively.

    Regions of high divergence are associated with \emph{sources} (e.g. where a fluid is being pumped in), and regions of low divergence are associated with \emph{sinks} (e.g. where a fluid drains out).
  \end{remark}
  \begin{theorem}[Divergence Theorem]
    Let $U \subseteq \R^3$ be a bounded domain whose boundary is a (piecewise) $C^1$ surface denoted by $\partial U$.
    If $v:U \to \R^3$ is a vector field, then
    \begin{equation*}
      \int_U (\dv v) \, dV = \oint_{\partial U} v \cdot \hat n \, dS,
    \end{equation*}
    where $\hat n$ is the outward pointing unit normal vector.
  \end{theorem}

  \begin{remark}
    Similar to our convention with line integrals, we denote surface integrals over \emph{closed surfaces} with the symbol $\oint$.
  \end{remark}

  \begin{remark}
    Let $B_R = B(x_0, R)$ and observe
    \begin{equation*}
      \lim_{R \to 0} \frac{1}{\vol(B_R)} \int_{\partial B_R} v \cdot \hat n \, dS
      = \lim_{R \to 0} \frac{1}{\vol(B_R)} \int_{B_R} \dv v \, dV = \dv v(x_0),
    \end{equation*}
    which justifies our intuition that $\dv v$ measures the outward flux of a vector field.
  \end{remark}

  \begin{remark}
    If $V \subseteq \R^2$, $U = V \times [a, b]$ is a cylinder, and $v:\R^3 \to R^3$ is a vector field that doesn't depend on $x_3$, then the divergence theorem reduces to Greens theorem.
  \end{remark}

  \begin{proof}[Proof of the divergence theorem]
    Suppose first that the domain $C$ is the unit cube $(0, 1)^3 \subseteq \R^3$.
    In this case
    \begin{equation*}
      \int_C \dv v \, dV
	= \int_C (\partial_1 v_1 + \partial_2 v_2 + \partial_3 v_3) \, dV.
    \end{equation*}
    Taking the first term on the right, the fundamental theorem of calculus gives
    \begin{align*}
      \int_C \partial_1 v_1 \, dV
	&= \int_{x_3 = 0}^1 \int_{x_2 = 0}^1 ( v_1(1, x_2, x_3) - v_1( 0, x_2, x_3) ) \, dx_2 \, d x_3
	\\
	&= \int_L v \cdot \hat n \, dS + \int_R v \cdot \hat n \, dS,
    \end{align*}
    where $L$ and $B$ are the left and right faces of the cube respectively.
    The $\partial_2 v_2$ and $\partial_3 v_3$ terms give the surface integrals over the other four faces.
    This proves the divergence theorem in the case that the domain is the unit cube.

    Now given an arbitrary domain $U$, suppose there exists a $C^2$ coordinate change function $\varphi: C \to U$.
    By interchanging $x_1$ and $x_2$ if necessary, we can guarantee $\det(D\varphi) > 0$ in all of $C$.
    Using the coordinate change formula for surface integrals observe
    \begin{equation*}
      \int_{\partial U} v \cdot \hat n \, dS
	= \int_{\partial C}
	    \paren[\big]{ \adj(D\varphi) v \circ \varphi } \cdot \hat n \, dS
	= \int_C \dv w \, dV,
    \end{equation*}
    where $\adj(D\varphi)$ is the adjunct of $D\varphi$, and
    \begin{equation*}
      w \defeq \adj(D\varphi) \, v \circ \varphi.
    \end{equation*}
    Now, with the product and chain rule, we claim
    \begin{equation}\label{e:dvw}
      \dv w = \det(D\varphi) \, (\dv v) \circ \varphi\,.
    \end{equation}
    Momentarily postponing the proof of~\eqref{e:dvw}, note that the coordinate change for volume integrals implies
    \begin{equation*}
      \int_{\partial U} v \cdot \hat n \, dS
      = \int_C \dv w \, dV
      = \int_U \dv v \, dV,
    \end{equation*}
    concluding the proof.
  \end{proof}

  It remains to prove~\eqref{e:dvw}.
  For clarity, we restate it as the following lemma.
  \begin{lemma}
    If $\varphi \colon \R^3 \to \R^3$ is $C^2$, and $v \colon \R^3 \to \R^3$ is $C^1$, then
    \begin{equation*}
      \dv \paren[\big]{ \adj(D\varphi) \, v \circ \varphi }
      = \det(D\varphi) \, (\dv v) \circ \varphi\,.
    \end{equation*}
  \end{lemma}
  \begin{proof}
    This can directly be brute force derived using the product rule, chain rule and Clairaut's theorem.
    However, the calculation is a little tedious if done directly.
    Here's a slightly more conceptual way of doing it.
    It's probably not any shorter, but you may find it easier to internalize.
    For convenience let $\adj(D\varphi) = (a_{i,j})$.
    By the product rule
    \begin{equation}\label{e:adj1}
      \dv \paren[\big]{ \adj(D\varphi) \, v \circ \varphi }
	= \sum_{i,j} \partial_i a_{i,j} v_j \circ \varphi
	  + \sum_{i,j} a_{i,j} \partial_i (v_j \circ \varphi)\,.
    \end{equation}
    We claim the first sum on the right vanishes, and the second sum gives us what we want.

    Let's study the second term first.
    By the chain rule,
    \begin{equation*}
      \sum_{i,j} a_{i,j} \partial_i (v_j \circ \varphi)
	= \sum_{i,j,k} a_{i,j} (\partial_k v_j) \circ \varphi \, \partial_i \varphi_k
	= \sum_{j, k} b_{k,j} (\partial_k v_j) \circ \varphi\,,
    \end{equation*}
    where
    \begin{equation*}
      b_{k,j} \defeq \sum_i a_{i,j}  \partial_i \varphi_k 
	= (D\varphi)_{k,i} \adj(D\varphi)_{i,j}\,.
    \end{equation*}
    This is simply the $k, j$-th entry of the matrix product $D\varphi \adj(D\varphi)$, and hence $b_{k,j} = \det(D\varphi)$ if $k = j$ and is $0$ otherwise.
    This immediately shows
    \begin{equation*}
      \sum_{i,j} a_{i,j} \partial_i (v_j \circ \varphi) = 
	\det(D\varphi) (\dv v) \circ \varphi\,,
    \end{equation*}
    as desired.

    Now let's study the first sum on the right of~\eqref{e:adj1}
    Note, by the formula for the adjunct,
    \begin{equation*}
      \adj(D\varphi) =
      \begin{pmatrix}
	\uparrow & \uparrow & \uparrow\\
	\grad \varphi_2 \times \grad \varphi_3
	  & \grad \varphi_3 \times \grad \varphi_1
	  & \grad \varphi_1 \times \grad \varphi_2
	\\
	\downarrow & \downarrow & \downarrow
      \end{pmatrix}
    \end{equation*}
    By the vector calculus identities
    \begin{equation*}
      \curl \grad f = 0
      \qquad\text{and}\qquad
      \dv( G \times H ) = (\curl G) \cdot H - G \cdot (\curl H)\,,
    \end{equation*}
    we see that each column of $\adj(D\varphi)$ has divergence $0$.
    (If you're not familiar with these identities, they can be quickly checked using Clairaut's theorem and the product rule.)
    This means for any $j$,
    \begin{equation*}
      \sum_i \partial_i a_{i,j} v_j \circ \varphi
	= \dv\paren[\big]{ \adj(D\varphi) e_j } v_j \circ \varphi
	= 0\,,
    \end{equation*}
    as desired.
    This finishes the proof.
  \end{proof}

  \begin{proposition}[Gauss's gravitational law]
    Let $g:\R^3 \to \R^3$ be the gravitational field of a mass distribution (i.e. $g(x)$ is the force experienced by a point mass located at $x$).
    If $\Sigma$ is any closed ($C^1$) surface, then
    \begin{equation*}
      \oint_\Sigma g \cdot \hat n \, dS = - 4 \pi G M,
    \end{equation*}
    where $M$ is the mass enclosed by the region $M$.
    Here $G$ is the gravitational constant, and $\hat n$ is the outward pointing unit normal vector.
  \end{proposition}

  \begin{proof}
    The crux of the matter is the following calculation.
    Given a fixed $y \in \R^3$, define the vector field $F$ by
    \begin{equation*}
      F(x) = \frac{x-y}{\abs{x - y}^3}.
    \end{equation*}
    Then
    \begin{equation}\label{eqnGauss1}
      \oint_\Sigma  F \cdot \hat n \, dS = 
      \begin{cases*}
	4\pi & if $y$ is in the region enclosed by $\Sigma$,\\
	0 & otherwise.
      \end{cases*}
    \end{equation}
    For simplicity, we subsequently assume $y = 0$.

    To prove~\eqref{eqnGauss1}, observe
    \begin{equation*}
      \dv F = 0,
    \end{equation*}
    when $x \neq 0$.
    Let $U$ be the region enclosed by $\Sigma$.
    If $0 \not\in U$, then the divergence theorem will apply to in the region $U$ and we have
    \begin{equation*}
      \oint_\Sigma g \cdot \hat n \, dS = \int_U \dv g \, dV = 0.
    \end{equation*}

    On the other hand, if $0 \in U$, the divergence theorem will not directly apply, since $F \not \in C^1(U)$.
    To circumvent this, let $\epsilon > 0$ and $U' = U - B(0, \epsilon)$, and $\Sigma'$ be the boundary of $U'$.
    Since $0 \not \in U'$, $F$ is $C^1$ on all of $U'$ and the divergence theorem  gives
    \begin{equation*}
      0 = \int_{U'} \dv F \, dV = \int_{\partial U'} F \cdot \hat n \, dS,
    \end{equation*}
    and hence
    \begin{equation*}
      \oint_{\Sigma} F \cdot \hat n \, dS
	= -\oint_{\partial B(0, \epsilon)} F \cdot \hat n \, dS
	= \oint_{\partial B(0, \epsilon)} \frac{1}{\epsilon^2} \, dS
	= -4\pi,
    \end{equation*}
    as claimed.
    (Above the normal vector on $\partial B(0, \epsilon)$ points outward with respect to the domain $U'$, and \emph{inward} with respect to the ball $B(0, \epsilon)$.)

    Now, in the general case, suppose the mass distribution has density $\rho$.
    Then  the gravitational field $g(x)$ will be the super-position of the gravitational fields at $x$ due to a point mass of size $\rho(y) \, dV$ placed at $y$.
    Namely, this means
    \begin{equation*}
      g(x) = -G \int_{\R^3} \frac{\rho(y) (x - y)}{\abs{x - y}^3} \, dV(y).
    \end{equation*}
    Now using Fubini's theorem,
    \begin{multline*}
      \int_\Sigma g(x) \cdot \hat n(x) \, dS(x)
	= -G \int_{y \in \R^3} \rho(y) \int_{x \in \Sigma} \frac{x -y}{\abs{x - y}^3} \cdot \hat n(x) \, dS(x) \, dV(y)
	\\
	= -4 \pi G \int_{y \in U} \rho(y) \, dV(y) = -4 \pi GM,
    \end{multline*}
    where the second last equality followed from~\eqref{eqnGauss1}.
  \end{proof}

  We saw earlier that $\curl (\grad V) = 0$, and conversely, in simply connected domains, any function for which $\curl F = 0$ must satisfy $F = -\grad V$ for some $V$.
  A similar result is true for the divergence and curl.

  \begin{proposition}
    For any $C^2$ vector field $F:\R^3  \to \R^3$ we must have
    \begin{equation*}
      \dv (\curl F) = 0.
    \end{equation*}
    Conversely, if $v:\R^3 \to \R^3$ is a $C^1$ vector field for which $\dv v = 0$, there must exist a $C^1$ vector field $F:\R^3 \to \R^3$ such that $v = \curl F$.
  \end{proposition}
  \begin{remark}
    If the vector field $v$ is only defined on a domain $U \subseteq \R^3$, then the above proposition is still true, \emph{provided} the domain~$U$ has ``no holes''.
    More precisely, for any closed surface $\Sigma \subseteq U$, the entire region enclosed by~$\Sigma$ must also be contained in~$U$.
  \end{remark}
  \begin{proof}
    Using Clairaut's theorem, we can directly check $\dv (\curl F) = 0$.
    For the converse, suppose $\dv v = 0$.
    We need to find a $C^1$ vector field~$F$ such that~$\curl F = v$.
    Note that $\curl( F + \grad \phi) = \curl F$ for any $C^2$ function~$\phi$.
    Set $G = F + \grad \phi$, and define
    \begin{equation*}
      \phi(x) = -\int_0^{x_3} F_3(x_1, x_2, t) \, dt\,,
    \end{equation*}
    and observe now that $G_3 = 0$.
    Thus if $v = \curl F$ for some $F$, we can always find a vector field $G$ such that $G_3 = 0$ and $v = \curl G$.
    Since this is simpler, we will directly construct a vector field~$G$, with $G_3 = 0$, such that $\curl G = v$.

    Note that if $G_3 = 0$, then
    \begin{equation*}
      \curl G = \colvec{-\partial_3 G_2\\ \partial_3 G_1 \\ \partial_1 G_2 - \partial_2 G_1}\,.
    \end{equation*}
    Since we want $\curl G = v$, we must have
    \begin{gather*}
      G_2(x) = -\int_0^{x_3} v_1(x_1, x_2, t) \, dt + C_2(x_1, x_2)\,,\\
      G_1(x) = \int_0^{x_3} v_2(x_1, x_2, t) \, dt + C_1(x_1, x_2)\,,
    \end{gather*}
    where $C_1$, $C_2$ are two functions that only depend on $x_1$ and $x_2$.
    This shows $(\curl G) \cdot e_i = v_i$ for $i = 1, 2$, and to finish the proof we only need to verify the same identity for $i = 3$.

    For this, recall $\dv v = 0$, and hence $\partial_3 v_3 = -\partial_1 v_1 - \partial_2 v_2$.
    Consequently,
    \begin{align*}
      \curl G \cdot e_3
	&= \partial_1 G_2 - \partial_2 G_1
	= \int_0^{x_3} (-\partial_1 v_2 - \partial_2 v_2) \, dt + \partial_1 C_2 - \partial_2 C_1
      \\
	&= \int_0^{x_3} \partial_3 v_3(x_1, x_2, t) \, dt + \partial_1 C_2 - \partial_2 C_1
      \\
	&= v_3(x_1, x_2, x_3) - v_3(x_1, x_2, 0) + \partial_1 C_2 - \partial_2 C_1\,.
    \end{align*}
    To finish the proof we only need to choose $C_1$ and $C_2$ so that
    \begin{equation*}
      v_3(x_1, x_2, 0) = \partial_1 C_2 - \partial_2 C_1\,.
    \end{equation*}
    This is easily arranged.
    Indeed, choose $C_1 = 0$ and
    \begin{equation*}
      C_2(x_1, x_2) = \int_0^t v_3(t, x_2, 0) \, dt\,.
    \end{equation*}
    With this choice of $G$, $C_1$ and $C_2$ we have $v = \curl G$ as desired.
  \end{proof}
\end{document}
