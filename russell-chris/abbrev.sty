%%%
% Author: Chris Almost
% Date:   18 Jan 2012
% This package contains all of the abbreviations I will be or might be using
% while typesetting the notes for the CMU math class 21-268.
%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{abbrev}[2012/04/25 LaTeX package for math abbreviations for 21-268.]

% AMS-LaTeX is required.
\RequirePackage{amsmath}

% Set some nice fonts.
\usepackage[charter]{mathdesign}
\usepackage[mathscr]{eucal}

% Use the hyperref package, without the stupid boxes around everything.
\usepackage[colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=red]{hyperref}
\hypersetup{pdfauthor={Chris Almost}}

%%% Define all the common theorem environments.

% Alas, hyperref does not play nice with the thref option.
\RequirePackage[amsmath,hyperref,thmmarks]{ntheorem}
%\RequirePackage[amsmath,hyperref,thref,thmmarks]{ntheorem}
\newcommand{\thref}[1]{\ref{#1}}

% bold title, italic body, no symbol
\theoremheaderfont{\normalfont\bfseries}
\theorembodyfont{\slshape}
\theoremsymbol{}
\theoremseparator{.}
% Theorem style environments are numbered together as <section>.<theorem>
% With the `nosection' option they are number sequentially.
% With the `nonumber' option they are not numbered.
\DeclareOption{nonumber}{\theoremstyle{nonumberplain}\newtheorem{theorem}{Theorem}}
\DeclareOption{nosection}{\theoremstyle{plain}\newtheorem{theorem}{Theorem}}
\ProcessOptions\relax
% If the `theorem' environment is still undefined then define it.
\ifthenelse{\isundefined{\theorem}}{\theoremstyle{change}\newtheorem{theorem}{Theorem}[section]}{}
% theorem-like environments
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{assumption}[theorem]{Assumption}
% normalfont body
\theorembodyfont{\normalfont}
% definition-like environments
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}

% Environment for lists of examples in the same style as the theorems.
\newtheorem{@examples}[theorem]{Examples}
\newenvironment{examples}%
{\begin{@examples}\mbox{}\@afterheading\begin{enumerate}}%
{\end{enumerate}\end{@examples}}

% Environment for lists of exercises in the same style as the theorems.
\newtheorem{@exercises}[theorem]{Exercises}
\newenvironment{exercises}%
{\begin{@exercises}\mbox{}\@afterheading\begin{enumerate}}%
{\end{enumerate}\end{@exercises}}

% Environment for lists of properties or whatnot in the same style as theorems.
\newenvironment{namedblock}[1]%
{\addvspace{\topsep}\addtocounter{theorem}{1}%
\noindent\textbf{\thetheorem\ #1.}\slshape}%
{\vspace*{1ex}}

\newenvironment{namedblock*}[1]%
{\addvspace{\topsep}\noindent\textbf{#1.}\slshape}%
{\vspace*{1ex}}

% italic title, unnumbered
\theoremstyle{nonumberplain}
\theoremheaderfont{\slshape}
% remark-like environments
\newtheorem{remark}{Remark}
\newtheorem{claim}{Claim}
\newtheorem{notation}{Notation}
\newtheorem{fact}{Fact}
% small caps title, colon, ends with a mark in the proper position.
\theoremheaderfont{\scshape}
\theoremseparator{:}
% proof-like environments
\theoremsymbol{\ensuremath{\square}}
\newtheorem{proof}{Proof}
\theoremsymbol{\ensuremath{\maltese}}
\newtheorem{solution}{Solution}

%%% The following commands exist solely because I am lazy and/or picky.

% Commands for preventing annoying breaks in theorem environments that begin
% with an enumerate or have a long optional argument.
\newcommand{\fixlist}{\mbox{}\@afterheading}
\newcommand{\fixthm}{\mbox{}\newline\@afterheading}

% Command for making snarky comments in the margin.
\newcommand{\marginnote}[1]{\marginpar{\footnotesize #1}}

% Command for defining new terms and adding index entries
\newcommand{\defining}[2][]{\index{#1#2}\emph{#2}}

% Command to record the lecture number and date (currently does nothing).
\newcounter{lecture}
%\newcommand{\lecture}[1]{\addtocounter{lecture}{1}%
%\noindent Lecture \arabic{lecture}, #1\hrule}
\newcommand{\lecture}[1]{}

% Define some useful operators.
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\convh}{conv} % convex hull
\DeclareMathOperator{\lspan}{span} % linear span
\DeclareMathOperator{\Div}{div}
\DeclareMathOperator{\curl}{curl}
\DeclareMathOperator{\area}{area}

\DeclareMathOperator*{\argmin}{argmin}

% Other useful abbreviations.
\newcommand{\cc}{\subset\!\subset}
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\pdsq}[2]{\frac{\partial^2 #1}{\partial #2^2}}
\newcommand{\pdsec}[3]{\frac{\partial^2 #1}{\partial #2\partial #3}}
\newcommand{\inner}[2]{\langle #1, #2\rangle}
% Vector fields are bold.
\renewcommand{\vec}[1]{\mathbf{#1}}

% Correct the symbol for the empty set from that horrible disfigured zero.
\renewcommand{\emptyset}{\varnothing}

% Introduce a shorter way to type the names of our favourite number systems.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}

% Command to write n^{th} without too much messing around.
% The optional argument changes the ``n'' to something else.
\newcommand{\nth}[1][n]{\ensuremath{#1^{\text{th}}}}

% Command for inverse ^{-1} without having to type any braces.
% The optional argument changes the ``1'' to something else.
\newcommand{\inv}[1][1]{\ensuremath{^{-#1}}}

% Command for stacking a word or symbol on top of an equals sign.
% The optional argument puts the word over some other binary relation.
\newcommand{\overeq}[2][=]{\stackrel{{}_{\text{#2}}}{#1}}
% Types of convergence of random variables.
\newcommand{\overto}[1]{\xrightarrow{{}_{\text{#1}}}}

% Quick commands for typing matrices (because they take long enough as is).

\newcommand{\mat}[1]{\begin{matrix} #1 \end{matrix}} %generic matrix
\newcommand{\bmat}[1]{\left[ \mat{#1} \right]}       %bracketed matrix
\newcommand{\pmat}[1]{\left( \mat{#1} \right)}       %parenthetical matrix
\newcommand{\dmat}[1]{\left| \mat{#1} \right|}       %determinant

\newcommand{\smat}[1]{\begin{smallmatrix} #1 \end{smallmatrix}} %small matrix
\newcommand{\bsmat}[1]{\left[ \smat{#1} \right]}  %bracketed small matrix
\newcommand{\psmat}[1]{\left( \smat{#1} \right)}  %parenthetical small matrix
\newcommand{\dsmat}[1]{\left| \smat{#1} \right|}  %small determinant

% Redefinitions of label commands to make them look better.
\renewcommand{\labelenumi}{\textup{\arabic{enumi}.}}
\renewcommand{\labelenumii}{\textup{(\alph{enumii})}}
\renewcommand{\labelitemi}{$\circ$}

